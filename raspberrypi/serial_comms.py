import serial
import time


port = serial.Serial()
port.port = '/dev/ttyUSB0'
port.baudrate = 9600
port.bytesize = 8
port.timeout = 5


def start_connection():
    try:
        port.open()
        answer=""
        while answer!=b'ack\r\n':
            port.write(b"start")
            answer = port.readline()
            
        return port
    except:
        return 0

def send_data(data_dict):
    for key, value in data_dict.items():
        answer=""
        licznik = 0
        p = bytes(key+" "+value+'\n', 'ascii')
        port.write(p)
        count=0
        print("Wysłano: ", p)
        while answer!=b'ack\r\n':
            licznik+=1
            answer = port.readline()
            time.sleep(1)
            if answer==b'':
                count+=1
            if count>5:
                print("log o nieudanym wysłaniu czegoś")
                break

    port.close()
