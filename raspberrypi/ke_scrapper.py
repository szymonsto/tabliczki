import requests
from bs4 import BeautifulSoup
import classes
import datetime, pytz
from tzlocal import get_localzone
import logging

def get_modification_time(url, f):

    # Get timezone in name and then in offset
    local_tz = get_localzone()
    gmt = datetime.datetime.now(pytz.timezone(str(local_tz))).strftime('%z')
    
    # Connect with URL and get time of last modification
    try:
        date = dict(f.headers)['Last-Modified']
    except:
        logging.error("Link %s does not have 'Last Modified' header.", url)
        return 0

    # Eliminating weekday from retrieved data
    date = date.split(', ')
    date.remove(date[0])

    # Dictionary which will help with translation of named month to its numeric value
    Months = {
        "Jan": "01",
        "Feb": "02",
        "Mar": "03",
        "Apr": "04",
        "May": "05",
        "Jun": "06",
        "Jul": "07",
        "Aug": "08",
        "Sep": "09",
        "Oct": "10",
        "Nov": "11",
        "Dec": "12"  
        }

    # Split remaining data to separate values: day, month, year, hour, gmtzone
    new_date = date[0].split()

    # Updating hour to the Europe/Warsaw timezone (+1/+2) by splitting into hour, minutes, seconds and updating hour, then getting them all together again
    hour = new_date[3]
    hour = hour.split(':')
    sign = gmt[:1]
    gmt = gmt[1:]
    gmt = int(gmt[:2])
    if sign=="-":
        hour[0] = int(hour[0])-gmt
    else:
        hour[0] = int(hour[0])+gmt
    hour = str(hour[0])+':'+hour[1]+':'+hour[2]

    # Forming a model data ready to compare
    new_date1 = new_date[2]+'-'+Months[new_date[1]]+'-'+new_date[0]
    new_date = [new_date1, hour]
    
    return new_date

def ke_scrapper (link0):
    objects = []
    base_link = link0
    
    #removes http://www. or https://www. from a base_link for example: http://www.iet.agh.edu.pl --> iet.agh.edu.pl, http://home.agh.edu.pl --> home.agh.edu.pl
    #or adds https:// if there is none in a base_link
    if base_link.find("https://") >= 0 or base_link.find("http://") >= 0:
        if base_link.find("www.") >= 0:
            shorten_base_link = base_link[base_link.find("www.")+4:]
        else:
            shorten_base_link = base_link[base_link.find("//")+2:]
    else:
        shorten_base_link = base_link
        base_link = "http://"+shorten_base_link
        
    link0 = base_link

    #Connect with and download link0
    try:
        f = requests.get(link0)
        f.encoding = "UTF-8"
    except:
        logging.error("Cannot connect to %s", link0)
        return 0


    #Use BS to group given text by html tags
    try:
        bs = BeautifulSoup(f.text, 'html.parser')
    except TypeError:
        logging.error("URL %s has made a TypeError, so it is being skipped", link0)
        return 0
    except UnboundLocalError:               #bug pythona w bibliotece BS4
        logging.warning("This URL caused python's BS4 library error. For the safety measures it is being skipped")
        return 0
    except:
        logging.error("URL %s caused an unexpected error. It will be skipped", link0)
        return 0
    
    # Search through all html's spans with name 'User input' - that's how i marked interesting data in template html
    results = bs.find_all('span', attrs = { 'name': 'User_input'})
    
    # If there are any results then:
    if results:
        # get last result which is actual consultation dates
        last_result = results[-1].text.split(',')
        hours = []
        days = []
        # Splitting days and hours, then putting them in appropriate lists
        for ind, res in enumerate(last_result):
            temp_res = res.split(' ')
            temp_res = list(filter(None, temp_res))
            last_result[ind] = (temp_res)
            hours.append(str(last_result[ind][0]))
            days.append(str(last_result[ind][1]))
        when = get_modification_time(link0, f)
        obj = classes.Teacher(str(results[5].text), str(results[3].text), hours, days, str(results[1].text), when)
        #obj.show_object()
        objects.append(obj)
    return objects

