import os
import platform
try:
    import paramiko
except:
    try:
        if platform.system()=="Windows":
            os.system("pip install paramiko")
        else:
            os.system("pip3 install paramiko")
        import paramiko
    except:
        raise SystemExit("Paramiko is not installed on this device and could not be installed properly. Try manual installation with 'pip install paramiko'/'pip3 install paramiko' depending on Windows/nonWindows OS")
       

def use_sftp (path, ip):

    try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, username='pi', password='pi')
    except:
        print("log ssh error")
        return 0


    try:
        sftp = ssh.open_sftp()
    except:
        print("sftp connection error log")
        return 0

    localpath = path

    filename_location = localpath.rfind('/')
    filename = localpath[filename_location:]

    remotepath = "/home/pi/doorlabels_sources"
    remotepath = remotepath + filename
    # Localpath is a string containing the path of the local file, remotepath is where the file should be copied on the raspberry pi

    try:
        print(localpath, remotepath)
        sftp.put(localpath, remotepath)
        sftp.close()
        ssh.close()
        return 1
    except:
        print("log sftp transmission error")
        return 0
