import requests
from bs4 import BeautifulSoup
import classes
import os
import logging
from datetime import datetime
import firebase_init


def is_url_on_list (link, url_objects):
    for obj in url_objects:
        if link == obj.address:
            return 1
    return 0

def starting_search (link):
    logging.info("Start searching URL: %s", link)
    
def ending_search (url_objects, link):
    logging.info("Searching of %s has ended.\nAmount of links that were checked: %s", link, (len(url_objects)))


# Searches Zbigniew Korzeń's site for last modified dates, as an argument takes a link to interesting subsite (plany_zima/plany_lato)
def search_for_modDate (link0):

    try:
        f = requests.get(link0)
        f.encoding = "UTF-8"
    except:
        logging.error("Cannot connect to %s", link0)
        return 0


    #Use BS to group given text by html tags
    try:
        bs = BeautifulSoup(f.text, 'html.parser')
    except TypeError:
        logging.error("URL %s has made a TypeError, so it is being skipped", link0)
        return 0
    except UnboundLocalError:               #bug pythona w bibliotece BS4
        logging.warning("This URL caused python's BS4 library error. For the safety measures it is being skipped")
        return 0
    except:
        logging.error("URL %s caused an unexpected error. It will be skipped", link0)
        return 0

    results = bs.find_all('td', attrs = {'align': 'right'})
    dates = []
    for ind, result in enumerate(results):
        if ind%2:
            dates.append(result.text)
            
    return dates



#searching a website for a given cryteria
def search_website_for_url (link, url_objects):
    
    base_link=link
    
    #removes http://www. or https://www. from a base_link for example: http://www.iet.agh.edu.pl --> iet.agh.edu.pl, http://home.agh.edu.pl --> home.agh.edu.pl
    if base_link.find("https://") >= 0 or base_link.find("http://") >= 0:
        if base_link.find("www.") >= 0:
            shorten_base_link = base_link[base_link.find("www.")+4:]
        else:
            shorten_base_link = base_link[base_link.find("//")+2:]
    else:
        shorten_base_link = base_link
        base_link = "http://"+shorten_base_link
        
    link0=base_link
    
    logging.info("URL being searched: %s", link)        
    
    #Connect with and download link0
    try:
        f = requests.get(link0)
    except:
        logging.error("URL %s is unreachable", link0)
        return 0

    #Use BS to group given text by html tags
    try:
        bs = BeautifulSoup(f.text, 'html.parser')
    except TypeError:
        logging.error("%s has made a TypeError when using BeautifulSoup, so it is being skipped", link0)
        return 0
    except UnboundLocalError:               #bug pythona w bibliotece BS4
        logging.error("%s caused python's BS4 library error. For the safety measures it is being skipped", link0)
        return 0
    except:
        logging.error("%s caused an unexpected error, which makes it skipped", link0)
        return 0

    #Brings out every URL from a website and puts it into the list called 'links'
    links = []
    for link in bs.find_all('a'):
        links.append(link.get('href'))

    #Eliminating 'None' from list
    links = list(filter(None, links))


    #checks if given url is still part of link0 base address, if not it is being put inside 'urls_to_delete" list, which then is used to remove those links from 'links'
    urls_to_delete = []

    for index, link in enumerate(links):
    
        if link[:4] == 'http':
            if link.find(base_link) < 0:
                urls_to_delete.append(link)
            
    for index, link in enumerate(urls_to_delete):
        links.remove(link)

    #Check if a href has full address, or just the ending like "/studenci"
    #If does - nothing happens, skips to next
    #If doesnt - adds link0 and the ending from href  
    for index, link in enumerate(links):
        if link[:4] == 'http':
            continue
        else:
            links[index] = base_link + link
            
    
    #create objects from links list
    #In addition : larger files (over 5MB of size) will be skipped to save time and resources
    #In addition2: searching_for_a_word function 
    for link in links:
        if is_url_on_list(link, url_objects) == 0:
            url_info = 9999999
            try:
                url_info_rq = requests.head(link)
                url_info = url_info_rq.headers.get('content-length', -1)
            except:
                logging.error("Cannot open the %s", link)
                continue
            if int(url_info) > 5000000:
                continue
            url = classes.URL_link(link, 0)
            url_objects.append(url)
            
            
    logging.info("Current amount of urls is: %s", len(url_objects))       
    
    
# Find specific file on Zbigniew Korzeń's site  and download it   
def get_file(names, urls, link):
    
    
    
    # First five hrefs are useless, lets remove them
    urls = urls[5:]
    # Get Dates
    dates = search_for_modDate(link)
    if len(urls)==len(dates):
        # For every URL check if link consists name that script is looking for (or names)
        for ind, url in enumerate(urls):
            for name in names:
                if str(url.address).find(name)>=0:
                    # If it does than downloads it to current folder...  
                    new_url = url.address.replace(link, '')
                    with open(new_url, 'wb') as file:
                        logging.info("Getting file %s", new_url)
                        response = requests.get(url.address)
                        file.write(response.content)
                        # Copies to 'doorlabels_sources'...  
                        os.system('sudo cp '+str(new_url)+' /home/pi/doorlabels_sources/')
                        # Removing in current folder...
                        os.system('sudo rm '+str(new_url))
                        # ... and changes its last-modified value to one given on website
                        dates[ind] = dates[ind].replace('-', '')
                        dates[ind] = dates[ind].replace(' ', '')
                        dates[ind] = dates[ind].replace(':', '')
                        dates[ind] = dates[ind]+'.00'
                        os.system('sudo touch -c -m -t ' + dates[ind] + ' /home/pi/doorlabels_sources/' + str(new_url))
                        logging.info("File named %s succesfully added.", new_url)
                        if not firebase_init.is_source("/home/pi/doorlabels_sources/"+str(new_url), "schedule"):
                            firebase_init.add_source("/home/pi/doorlabels_sources/"+str(new_url), "schedule")
    else:
        logging.error("Length of URLs list is not the same as data's")
        
        
# Enters every url on list in order to search suburls
def build_url_tree (url_objects):
    print("1 URL has been checked")
    for index, obj in enumerate(url_objects):
        if obj.status == 0:
            obj.status = 1
            search_website_for_url(obj.address, url_objects, base_link)     
            print(str(index+2) + " URLs have been checked")
            
# Schedules appear on two different subsites: plany_zima & plany_lato
# This function's role is to extract a correct one given current date
def get_website():
    links = ["http://home.agh.edu.pl/~kozby/ke/plany_zajec_zima/",
             "http://home.agh.edu.pl/~kozby/ke/plany_zajec_lato/"]
    
    today= datetime.now()
    month = int(today.strftime("%m"))
    day = int(today.strftime("%d"))

    # bigger means younger
    if month > 10 or (month>=1 and (month<=2 and day<=15)):
        logging.info("Extracted %s in order to begin search", links[0])
        return links[0]
    else:
        logging.info("Extracted %s in order to begin search", links[0])
        return links[1]
        
    


