#!/usr/bin/python3

import os
import platform
import serial_comms

try:
    import tkinter
except:
    try:
        if platform.system()=="Windows":
            os.system("pip install tkinter")
        else:
            os.system("pip3 install tkinter")
        import tkinter
    except:
        raise SystemExit("Tkinter is not installed on this device and could not be installed properly. Try manual installation with 'pip install tkinter'/'pip3 install tkinter' depending on Windows/nonWindows OS")
try:
    import pyrebase
except:
    try:
        if platform.system()=="Windows":
            os.system("pip install pyrebase4")
        else:
            os.system("pip3 install pyrebase4")
        import pyrebase
    except:
        raise SystemExit("Pyrebase is not installed on this device and could not be installed properly. Try manual installation with 'pip install pyrebase4'/'pip3 install pyrebase4' depending on Windows/nonWindows OS")
import sftp

def connect_db ():
    configuration = {
        "apiKey": "AIzaSyBPKwn6GHk65vZbHDJm04QZhLDtOO4uemA",
        "authDomain": "tabliczki-dfd63.firebaseapp.com",
        "databaseURL": "https://tabliczki-dfd63.firebaseio.com",
        "projectId": "tabliczki-dfd63",
        "storageBucket": "tabliczki-dfd63.appspot.com",
        "messagingSenderId": "1037582045549",
        "appId": "1:1037582045549:web:79ab799bc2e617eb53ca64"
    }
    firebase = pyrebase.initialize_app(configuration)
    db = firebase.database()
    return db

def read_ip():
    try:
        ip = db.child("Rasp Pi").child("IP").get()
        return ip.val()
    except:
        return "Getting IP from FireBase had failed"


def read_sources(req, text):
    try:
        text.delete('1.0', tkinter.END)
        if isinstance(req, list):
            if req[0]==0:
                source = db.child("Sources").child("MS Excel Files").get().val().items()
                vals = []
                for k, v in source:
                    vals.append(v)
                for ind, value in enumerate(vals):
                    text.insert(str(float(ind+1)), str(value)+'\n')              
            elif req[0]==1:
                source = db.child("Sources").child("Google Sheets").get().val().items()
                vals = []
                for k, v in source:
                    vals.append(v)
                for ind, value in enumerate(vals):
                    text.insert(str(float(ind+1)), str(value)+'\n')
            elif req[0]==2:
                source = db.child("Sources").child("HTML Links").get().val().items()
                vals = []
                for k, v in source:
                    vals.append(v)
                for ind, value in enumerate(vals):
                    text.insert(str(float(ind+1)), str(value)+'\n')
            elif req[0]==3:
                source = db.child("Sources").child("AGH Schedules").get().val().items()
                vals = []
                for k, v in source:
                    vals.append(v)
                for ind, value in enumerate(vals):
                    text.insert(str(float(ind+1)), str(value)+'\n')
        else:
            return 0
    except:
        print("Objects reading failed: READ SOURCES")
        return 0

def put_source (entry, source, pane, coordinates):
    try:
        if isinstance(source, list):
            if source[0]==2:
                file = entry.get()[entry.get().rfind('/')+1:]
                path = "/home/pi/doorlabels_sources/"+file
                db.child('Sources').child('AGH Schedules').push(path)
            elif source[0]==0:
                db.child('Sources').child('Google Sheets').push(entry.get())
            elif source[0]==1:
                db.child("Sources").child("HTML Links").push(entry.get())
            elif source[0]==3:
                file = entry.get()[entry.get().rfind('/')+1:]
                path = "/home/pi/doorlabels_sources/"+file
                db.child('Sources').child('MS Excel Files').push(path)
        else:
            label = tkinter.Label(pane, text='Zaznacz rodzaj i uzupełnij lukę!', bg = '#c2b3a8', font='48', fg='red')
            label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3]) 
            return 0

        label = tkinter.Label(pane, text='Udało się poprawnie wpisać do bazy danych!', bg = '#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3]) 
        entry.delete(0,len(entry.get()))
    except:
        label = tkinter.Label(pane, text='Nie udało się poprawnie wpisać do bazy danych! Put_source', bg = '#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3]) 
        
def remove_source (entry, source, pane):
    try:
        if isinstance(source, list):
            if source[0]==0:
                data = db.child("Sources").child("AGH Schedules").get().val().items()
            elif source[0]==1:
                data = db.child("Sources").child("Google Sheets").get().val().items()
            elif source[0]==2:
                data = db.child("Sources").child("HTML Links").get().val().items()
            elif source[0]==3:
                data = db.child("Sources").child("MS Excel Files").get().val().items()
        else:
            label = tkinter.Label(pane, text='Zaznacz rodzaj i uzupełnij lukę!', bg = '#c2b3a8', font='48', fg='red')
            label.place(relx=0.1, rely=0.6, relwidth=0.8, relheight=0.05)
            return 0
            
        flag=0
        for key,val in data:
            if val==entry.get():
                if source[0]==0:
                    db.child("Sources").child("AGH Schedules").child(key).remove()
                elif source[0]==1:
                    db.child("Sources").child("Google Sheets").child(key).remove()
                elif source[0]==2:
                    db.child("Sources").child("HTML Links").child(key).remove()
                elif source[0]==3:
                    db.child("Sources").child("MS Excel Files").child(key).remove()
                flag=1
        if not flag:
            label = tkinter.Label(pane, text='Nie ma takiego wpisu w bazie danych!', bg = '#c2b3a8', font='48', fg='black')
            label.place(relx=0.1, rely=0.6, relwidth=0.8, relheight=0.05)
            return 0

        label = tkinter.Label(pane, text='Udało się poprawnie usunąc wpis z bazy danych!', bg = '#c2b3a8', font='48', fg='green')
        label.place(relx=0.1, rely=0.6, relwidth=0.8, relheight=0.05)
        entry.delete(0,len(entry.get()))
    except:
        label = tkinter.Label(pane, text='Nie udało się poprawnie usunąc wpisu z bazy danych!', bg = '#c2b3a8', font='48', fg='red')
        label.place(relx=0.1, rely=0.6, relwidth=0.8, relheight=0.05)

def put_string_into_label (string, label):
    label["text"] = string

def check_list_select (listbox, pane, coordinates):
    try:
        index = listbox.curselection()[0]
        name = listbox.get(index)
        if name=="No ESPs in database":
            return -1
        label = tkinter.Label(pane, bg = '#c2b3a8')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        output = [index, name]
        return output
    except:
        label = tkinter.Label(pane, text='Zaznacz rodzaj!', bg = '#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        return -1
        

def connect_via_serial (button, text, pane):
    button["state"] = "disabled"
    text.insert('1.0', "Connection starting...\n")
    if serial_comms.start_connection():
        text.insert('2.0', "Connection has been established successfully")
        #time.sleep(5) #buffered output sprawia, ze najpierw jest pauza, a potem wszystkie wpisy
        PaneSwitcher(21, pane)
    else:
        text.insert('2.0', "Connection has not been established successfully")
        button["state"] = "normal"
        
def update_serial_pane(pane, entries):
    returns = []
    if entries[0]==entries[1] and entries[0]!="":
        label = tkinter.Label(pane, text='Hasła pasują do siebie!', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=0.1, rely=0.36, relwidth=0.8, relheight=0.03)
        returns.append(1)
    elif entries[0]!=entries[1]:
        label = tkinter.Label(pane, text='Hasła nie pasują do siebie!', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=0.1, rely=0.36, relwidth=0.8, relheight=0.03)
        returns.append(0)
    else:
        label = tkinter.Label(pane, bg='#c2b3a8')
        label.place(relx=0.1, rely=0.36, relwidth=0.8, relheight=0.03)
        returns.append(1)
        
    if entries[2]==entries[3] and entries[2]!="":
        label = tkinter.Label(pane, text='Hasła pasują do siebie!', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=0.1, rely=0.66, relwidth=0.8, relheight=0.03)
        returns.append(1)
    elif entries[2]!=entries[3]:
        label = tkinter.Label(pane, text='Hasła nie pasują do siebie!', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=0.1, rely=0.66, relwidth=0.8, relheight=0.03)
        returns.append(0)
    else:
        label = tkinter.Label(pane, bg='#c2b3a8')
        label.place(relx=0.1, rely=0.66, relwidth=0.8, relheight=0.03)
        returns.append(1)
    
    return returns
        
def send_via_serial (pane, entries):
    passwords_match = update_serial_pane(pane, [entries[1], entries[2], entries[4], entries[5]])
    passwords_match = passwords_match[0] and passwords_match[1]
    entries_fulfilled = entries[0]!="" and entries[3]!="" and entries[6]!="" and entries[7]!="" and entries[8]!=""
    if passwords_match and entries_fulfilled:
        label1 = tkinter.Label(pane, text="Trwa wysyłanie, proszę czekać", bg='#c2b3a8', font='48', fg='orange')
        label1.place(relx=0.65, rely=0.9, relwidth=0.3, relheight=0.05)
        data = {
            "ap_ssid": entries[0],
            "ap_passwd": entries[1],
            "dev_name": entries[3],
            "dev_passwd": entries[4],
            "base_key": entries[6],
            "base_auth": entries[7],
            "room": entries[8]
            }
        try:
            serial_comms.send_data(data)
            label1 = tkinter.Label(pane, bg='#c2b3a8')
            label1.place(relx=0.65, rely=0.9, relwidth=0.3, relheight=0.05)
            label = tkinter.Label(pane, text="Konfiguracja przesłana pomyślnie", bg='#c2b3a8', font='48', fg='green')
            label.place(relx=0.1, rely=0.86, relwidth=0.8, relheight=0.03)
            PaneSwitcher(21, pane)
        except:
            label1 = tkinter.Label(pane, bg='#c2b3a8')
            label1.place(relx=0.65, rely=0.9, relwidth=0.3, relheight=0.05)
            label = tkinter.Label(pane, text="Konfiguracji nie udało się przesłać pomyślnie - sprawdź połączenie", bg='#c2b3a8', font='48', fg='red')
            label.place(relx=0.1, rely=0.86, relwidth=0.8, relheight=0.03)
            print("Nie udało wysłać się po porcie szeregowym")
    else:
        label = tkinter.Label(pane, text="Uzupełnij wszystkie pola!", bg='#c2b3a8', font='48', fg='red')
        label.place(relx=0.1, rely=0.86, relwidth=0.8, relheight=0.03)
            
        
def update_esp_config (name, configs, pane, coordinates):
    try:
        old_config = read_esp_config(str(name))
        print(old_config)
        config_dict = {
            "Battery": old_config["Battery"],
            "Power Status": old_config["Power Status"],
            "Connected to": old_config["Connected to"],
            "Is Repeater": old_config["Is Repeater"]
            }
        print(2)
        config_names = ["refreshing frequency", "Device SSID", "Room number", "Device password"]
        print(3)
        for index, config in enumerate(configs):
            print(4)
            if config != "":
                print(51)
                config_dict.update({config_names[index]:config})
                print(52)
            else:
                config_dict.update({config_names[index]:old_config[config_names[index]]})
        
        if name != config_dict["Device SSID"]:
            db.child("ESP").child(str(config_dict["Device SSID"])).set(config_dict)
            db.child("ESP").child(name).remove()
        else:
            db.child("ESP").child(name).set(config_dict)
        
        PaneSwitcher(31, pane, [0, config_dict["Device SSID"]])
        label = tkinter.Label(pane, text='Edycja ESP w bazie danych zakończona sukcesem', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Edycja ESP w bazie danych nie powiodła się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: wpisywanie esp do FB")
        
def remove_esp_config (ESP_index_name, pane, coordinates):
    try:
        name = ESP_index_name[1]
        db.child("ESP").child(str(name)).remove()
        PaneSwitcher(3, pane)
    except:
        label = tkinter.Label(pane, text='Usuwanie ESP z bazy nie powiodło się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    
        
def add_esp(name, configs, pane, coordinates):
    try:
        config_dict = {
            "refreshing frequency": configs[0],
            "Device SSID": configs[1],
            "Room number": configs[2],
            "Device password": configs[3],
            "Battery": "",
            "Power Status": "OFF",
            "Connected to": "",
            "Is Repeater": "YES"
            }
        db.child("ESP").child(str(name)).set(config_dict)
        PaneSwitcher(33, pane, [0, config_dict["Device SSID"]])
        label = tkinter.Label(pane, text='Dodawanie ESP w bazie danych zakończone sukcesem', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Dodawanie ESP w bazie danych nie powiodło się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: wpisywanie esp do FB")
        

def read_esps():
    try:
        data = db.child("ESP").get().val().items()
        output_data = []
        for key, val in data:
            output_data.append(key)
        return output_data
    except:
        print("tu będą logi")
        

def read_esp_config (name):
    try:
        data = db.child("ESP").child(str(name)).get().val().items()
        new_dict = {}
        for key, val in data:
            new_dict.update({key: val})
        return new_dict
    except:
        print("tu będą logi")
        
def show_esp_config (text, name, pane):
    try:
        text.delete('1.0', tkinter.END)
        if isinstance(name, list):
            esp = read_esp_config(name[1])
            for key, value in esp.items():
                text.insert('1.0', key+": "+value+"\n")
            cleaning_label = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label.place(relx=0.1, rely=0.35, relwidth=0.8, relheight=0.05)
            cleaning_label1 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label1.place(relx=0.1, rely=0.45, relwidth=0.8, relheight=0.05)
            cleaning_label2 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label2.place(relx=0.1, rely=0.55, relwidth=0.8, relheight=0.05)
            cleaning_label3 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label3.place(relx=0.1, rely=0.65, relwidth=0.8, relheight=0.05)
            cleaning_label4 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label4.place(relx=0.1, rely=0.75, relwidth=0.8, relheight=0.05)
            cleaning_label5 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label5.place(relx=0.1, rely=0.85, relwidth=0.8, relheight=0.05)
        else:
            return 0
    except:
        print("nie udało się pokazac")
        
def esp_switch_power (name, pane, coordinates):
    try:
        states = ["OFF", "ON"]
        status = db.child("ESP").child(str(name[1])).child("Power Status").get().val()
        states.remove(status)
        new_status = states[0]
        if isinstance(name, list):
            db.child("ESP").child(str(name[1])).child("Power Status").set(str(new_status))
            label = tkinter.Label(pane, text='Status urządzenia zmieniony pomyślnie!', bg='#c2b3a8', font='48', fg='green')
            label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        else:
            label = tkinter.Label(pane, text='Zaznacz urządzenie!', bg = '#c2b3a8', font='48', fg='red')
            label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
            return 0
    except:
        label = tkinter.Label(pane, text='Zmiana statusu urządzenia nie udała się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        
def esp_switch_repeater (name, pane, coordinates):
    try:
        states = ["NO", "YES"]
        status = db.child("ESP").child(str(name[1])).child("Is Repeater").get().val()
        states.remove(status)
        new_status = states[0]
        if isinstance(name, list):
            db.child("ESP").child(str(name[1])).child("Is Repeater").set(str(new_status))
            label = tkinter.Label(pane, text='Status urządzenia zmieniony pomyślnie!', bg='#c2b3a8', font='48', fg='green')
            label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        else:
            label = tkinter.Label(pane, text='Zaznacz urządzenie!', bg = '#c2b3a8', font='48', fg='red')
            label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
            return 0
    except:
        label = tkinter.Label(pane, text='Zmiana statusu urządzenia nie udała się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        
def read_aps():
    try:
        data = db.child("AP").get().val().items()
        output_data = []
        for key, val in data:
            output_data.append(key)
        return output_data
    except:
        print("tu będą logi")
        
def read_ap_config (name):
    try:
        data = db.child("AP").child(str(name)).get().val().items()
        new_dict = {}
        for key, val in data:
            new_dict.update({key: val})
        return new_dict
    except:
        print("tu będą logi")
        
def remove_ap (AP_index_name, pane, coordinates):
    try:
        name = AP_index_name[1]
        db.child("AP").child(str(name)).remove()
        PaneSwitcher(5, pane)
    except:
        label = tkinter.Label(pane, text='Usuwanie AP z bazy nie powiodło się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        
def add_ap(name, configs, pane, coordinates):
    try:
        config_dict = {
            "AP SSID": configs[0],
            "AP password": configs[1]
            }
        db.child("AP").child(str(name)).set(config_dict)
        PaneSwitcher(53, pane, [0, config_dict["AP SSID"]])
        label = tkinter.Label(pane, text='Dodawanie AP w bazie danych zakończone sukcesem', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Dodawanie AP w bazie danych nie powiodło się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: wpisywanie esp do FB")
        
def show_aps (text, name, pane):
    try:
        text.delete('1.0', tkinter.END)
        if isinstance(name, list):
            ap = read_ap_config(name[1])
            for key, value in ap.items():
                text.insert('1.0', key+": "+value+"\n")
            cleaning_label = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label.place(relx=0.1, rely=0.35, relwidth=0.8, relheight=0.05)
            cleaning_label1 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label1.place(relx=0.1, rely=0.45, relwidth=0.8, relheight=0.05)
            cleaning_label2 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label2.place(relx=0.1, rely=0.55, relwidth=0.8, relheight=0.05)
            cleaning_label3 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label3.place(relx=0.1, rely=0.65, relwidth=0.8, relheight=0.05)
            cleaning_label4 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label4.place(relx=0.1, rely=0.75, relwidth=0.8, relheight=0.05)
            cleaning_label5 = tkinter.Label(pane, bg = '#c2b3a8')
            cleaning_label5.place(relx=0.1, rely=0.85, relwidth=0.8, relheight=0.05)
        else:
            return 0
    except:
        print("nie udało się pokazac ap")
        
def update_ap_config (name, configs, pane, coordinates):
    try:
        old_config = read_ap_config(str(name))
        config_dict = { }        
        config_names = ["AP SSID", "AP password"]
        for index, config in enumerate(configs):
            if config != "":      
                config_dict.update({config_names[index]:config})
            else:
                config_dict.update({config_names[index]:old_config[config_names[index]]})
        
        if name != config_dict["AP SSID"]:
            db.child("AP").child(str(config_dict["AP SSID"])).set(config_dict)
            db.child("AP").child(name).remove()
        else:
            db.child("AP").child(name).set(config_dict)
        
        PaneSwitcher(51, pane, [0, config_dict["AP SSID"]])
        label = tkinter.Label(pane, text='Edycja AP w bazie danych zakończona sukcesem', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Edycja AP w bazie danych nie powiodła się', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: wpisywanie ap do FB")
        
        
def show_raspberry_config (text, pane, coordinates):
    try:
        pi = db.child("Rasp Pi").get().val().items()
        text.delete('1.0', tkinter.END)
        for key, value in pi:
            text.insert('1.0', key+":  "+value+"\n")
        label = tkinter.Label(pane, bg='#c2b3a8')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Nie udało się zdobyć informacji o Raspberry Pi', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: wyswietlanie rasp")
        
def update_raspberry_config (user_or_pass, data, pane, coordinates):
    try:
        cleaning_label = tkinter.Label(pane, bg = '#c2b3a8')
        cleaning_label.place(relx=0.1, rely=0.16, relwidth=0.8, relheight=0.03)
        cleaning_label1 = tkinter.Label(pane, bg = '#c2b3a8')
        cleaning_label1.place(relx=0.1, rely=0.36, relwidth=0.8, relheight=0.03)
        db.child("Rasp Pi").child(user_or_pass).set(data)
        label = tkinter.Label(pane, text='Dana zaktualizowana pomyślnie', bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
    except:
        label = tkinter.Label(pane, text='Nie udało się zaktualizować', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        print("tu będą logi: update rasp")
        
def write_file_into_raspberry (path, pane, coordinates):
    try:
        ip = db.child("Rasp Pi").child("IP").get().val()
    except:
        print("logi, ze ip niezczytane")
        return 0
    if sftp.use_sftp(path, ip):
        PaneSwitcher(12, pane)
        label = tkinter.Label (pane, text="Pomyślnie dodano plik", bg='#c2b3a8', font='48', fg='green')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        return 1
    else:
        label = tkinter.Label(pane, text='Nie udało się dodać pliku', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        return 0
        
def write_file (entry, pane, coordinates, source):
    if isinstance(source, list):
        if source[0]==1:
            source[0]=3
        elif source[0]==0:
            source[0]=2
    else:
        return 0
            
    if write_file_into_raspberry (entry.get(), pane, coordinates):
        coordinates1 = [coordinates[0], coordinates[1]+0.05, coordinates[2], coordinates[3]]
        put_source(entry, source, pane, coordinates1)
    else:
        label = tkinter.Label(pane, text='Nie udało się dodać pliku', bg='#c2b3a8', font='48', fg='red')
        label.place(relx=coordinates[0], rely=coordinates[1], relwidth=coordinates[2], relheight=coordinates[3])
        
def PaneSwitcher(number, frame, *args):
    if len(args):
        if args[0]==-1:
            return 0
    
    if number==1:
        pane1 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane1.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        label11 = tkinter.Label(pane1, text="Obecna konfiguracja raspberry: ", bg = '#c2b3a8', font='48', fg='black')
        label11.place(relx=0.1, rely=0.075, relwidth=0.4, relheight=0.05)
        
        text11 = tkinter.Text(pane1)
        text11.place(relx=0.5, rely=0.05, relwidth=0.3, relheight=0.1)
        
        button11 = tkinter.Button (pane1, text='Pokaż konfigurację', command = lambda: show_raspberry_config(text11, pane1, [0.1, 0.26, 0.8, 0.03]))
        button11.place(relx=0.3, rely=0.2, relwidth=0.4, relheight=0.05)
        
        button12 = tkinter.Button (pane1, text='Zmień dane uwierzytelniające', command = lambda: PaneSwitcher(11, pane1))
        button12.place(relx=0.3, rely=0.3, relwidth=0.4, relheight=0.05)
        
        button13 = tkinter.Button (pane1, text='Prześlij plik źródłowy', command = lambda: PaneSwitcher(12, pane1))
        button13.place(relx=0.3, rely=0.4, relwidth=0.4, relheight=0.05)
        
    elif number==11:
        pane11 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane11.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        label111 = tkinter.Label(pane11, text='Nowa nazwa użytkownika',bg = '#c2b3a8')
        label111.place(relx=0.1, rely=0.1, relwidth=0.3, relheight=0.05)
        entry111 = tkinter.Entry(pane11)
        entry111.place(relx=0.45, rely=0.1, relwidth=0.3, relheight=0.05)
        button111 = tkinter.Button(pane11, text='Zmień', command = lambda: update_raspberry_config("username", entry111.get(), pane11, [0.1, 0.16, 0.8, 0.03]))
        button111.place(relx=0.8, rely=0.1, relwidth=0.15, relheight=0.05)
        
        label112 = tkinter.Label(pane11, text='Nowe hasło',bg = '#c2b3a8')
        label112.place(relx=0.1, rely=0.3, relwidth=0.3, relheight=0.05)
        entry112 = tkinter.Entry(pane11, show='*')
        entry112.place(relx=0.45, rely=0.3, relwidth=0.3, relheight=0.05)
        button112 = tkinter.Button(pane11, text='Zmień', command = lambda: update_raspberry_config("password", entry112.get(), pane11, [0.1, 0.36, 0.8, 0.03]))
        button112.place(relx=0.8, rely=0.3, relwidth=0.15, relheight=0.05)
        
        button113 = tkinter.Button(pane11, text='Wróć', command = lambda: PaneSwitcher(1, frame1))
        button113.place(relx=0.6, rely=0.5, relwidth=0.2, relheight=0.05)
        
    elif number==12:
        pane12 = tkinter.PanedWindow (frame, bg = '#c2b3a8')
        pane12.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        label121 = tkinter.Label (pane12, text='Wprowadź ścieżkę do pliku, który chcesz przesłać', bg = '#c2b3a8')
        label121.place(relx=0.1, rely=0.2, relwidth=0.8, relheight=0.05)
        
        listbox121 = tkinter.Listbox (pane12, selectmode='single', height=str(0))
        listbox121.place(relx=0.4, rely=0.25, relwidth=0.2, relheight=0.1)
        listbox121.insert(1, "Plan AGH")
        listbox121.insert(2, "Arkusz Excel")
        
        entry121 = tkinter.Entry (pane12)
        entry121.place(relx=0.1, rely=0.4, relwidth=0.8, relheight=0.05)
        
        button121 = tkinter.Button (pane12, text='Dodaj', command = lambda: write_file(entry121, pane12, [0.1, 0.56, 0.8, 0.03], check_list_select(listbox121, pane12, [0.1, 0.56, 0.8, 0.03])))
        button121.place(relx=0.3, rely=0.5, relwidth=0.4, relheight=0.05)
        
        button122 = tkinter.Button (pane12, text='Wróć', command = lambda: PaneSwitcher(1, frame1))
        button122.place(relx=0.3, rely=0.65, relwidth=0.4, relheight=0.05)
        
        text121 = tkinter.Text (pane12, bg = '#c2b3a8')
        text121.place(relx=0.1, rely=0.75, relwidth=0.8, relheight=0.1)
        text121.insert('1.0', "UWAGA: podawać należy ścieżkę bezwględną do pliku na swoim komputerze, np.\n")
        text121.insert('2.0', "Windows: C:/Users/Michał/Dokumenty/plik.xls \n")
        text121.insert('3.0', "Raspbian: /home/pi/Documents/plik.xls \n")
        text121.insert('3.0', "Resztą zajmie się program i powiadomi o wyniku")
        
    elif number==2:
        pane2 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane2.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        text21 = tkinter.Text(pane2, bg = 'white')
        text21.place(relx=0.3, rely=0.3, relwidth=0.4, relheight=0.4)
        
        button21 = tkinter.Button(pane2, bg = 'grey', text = 'Connect', command = lambda: connect_via_serial(button21, text21, pane2) )
        button21.place(relx=0.4, rely=0.1, relwidth=0.2, relheight=0.1)
        
    elif number == 21:
        pane21 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane21.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        entry210 = tkinter.Entry(pane21)
        entry210.place(relx=0.45, rely=0.01, relwidth=0.5, relheight=0.05)
        label210 = tkinter.Label(pane21, bg = '#c2b3a8', text = "Set Room Number: ", font = '16')
        label210.place(relx=0.01, rely=0.01, relwidth=0.4, relheight=0.05)
        
        entry211 = tkinter.Entry(pane21)
        entry211.place(relx=0.45, rely=0.1, relwidth=0.5, relheight=0.05)
        label211 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'AP ssid: ', font = '16')
        label211.place(relx=0.01, rely=0.1, relwidth=0.4, relheight=0.05)
        
        entry212 = tkinter.Entry(pane21, show='*')
        entry212.place(relx=0.45, rely=0.2, relwidth=0.5, relheight=0.05)
        label212 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'AP password: ', font = '16')
        label212.place(relx=0.01, rely=0.2, relwidth=0.4, relheight=0.05)
        
        entry213 = tkinter.Entry(pane21, show='*')
        entry213.place(relx=0.45, rely=0.3, relwidth=0.5, relheight=0.05)
        label213 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'Enter AP password again: ', font = '16')
        label213.place(relx=0.01, rely=0.3, relwidth=0.4, relheight=0.05)
        
        entry214 = tkinter.Entry(pane21)
        entry214.place(relx=0.45, rely=0.4, relwidth=0.5, relheight=0.05)
        label214 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'Set Device Name: ', font = '16')
        label214.place(relx=0.01, rely=0.4, relwidth=0.4, relheight=0.05)
        
        entry215 = tkinter.Entry(pane21, show='*')
        entry215.place(relx=0.45, rely=0.5, relwidth=0.5, relheight=0.05)
        label215 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'Set Device Password: ', font = '16')
        label215.place(relx=0.01, rely=0.5, relwidth=0.4, relheight=0.05)
        
        entry216 = tkinter.Entry(pane21, show='*')
        entry216.place(relx=0.45, rely=0.6, relwidth=0.5, relheight=0.05)
        label216 = tkinter.Label(pane21, bg = '#c2b3a8', text = 'Enter device password again: ', font = '16')
        label216.place(relx=0.01, rely=0.6, relwidth=0.4, relheight=0.05)
        
        entry217 = tkinter.Entry(pane21)
        entry217.place(relx=0.45, rely=0.7, relwidth=0.5, relheight=0.05)
        label217 = tkinter.Label(pane21, bg = '#c2b3a8', text = "Set Firebase's apiKey: ", font = '16')
        label217.place(relx=0.01, rely=0.7, relwidth=0.4, relheight=0.05)
        
        entry218 = tkinter.Entry(pane21)
        entry218.place(relx=0.45, rely=0.8, relwidth=0.5, relheight=0.05)
        label218 = tkinter.Label(pane21, bg = '#c2b3a8', text = "Set Firebase's authDomain: ", font = '16')
        label218.place(relx=0.01, rely=0.8, relwidth=0.4, relheight=0.05)
        
        button211 = tkinter.Button(pane21, text = 'Send', command = lambda: send_via_serial(pane21, [entry211.get(),entry212.get(),entry213.get(),entry214.get(),entry215.get(),entry216.get(),entry217.get(),entry218.get(),entry210.get()]))
        button211.place(relx=0.4, rely=0.9, relwidth=0.2, relheight=0.05)
        
    elif number==3:
        pane3 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane3.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        try:
            esps = read_esps()
            len_esps = len(esps)
        except:
            esps = ["No ESPs in database"]
        
        listbox31 = tkinter.Listbox(pane3, selectmode='single', height=str(0))
        listbox31.place(relx=0.1, rely=0.05, relwidth=0.2, relheight=0.2)
        for index, esp in enumerate(esps):
            listbox31.insert(index+1, str(esp))
        
        text31 = tkinter.Text(pane3)
        text31.place(relx=0.4, rely=0.05, relwidth = 0.5, relheight=0.2)
        
        cds = [0.1, 0.26, 0.8, 0.03]
        button31 = tkinter.Button(pane3, text='Ustaw konfigurację', command = lambda: PaneSwitcher(31, pane3, check_list_select(listbox31, pane3, cds)))
        button31.place(relx=0.3, rely=0.4, relwidth=0.4, relheight=0.05)
        
        button32 = tkinter.Button(pane3, text='Usuń ESP z bazy danych', command = lambda: remove_esp_config(check_list_select(listbox31,pane3,cds), pane3, [0.1, 0.76, 0.8, 0.03]))
        button32.place(relx=0.3, rely=0.7, relwidth=0.4, relheight=0.05)
        
        button33 = tkinter.Button(pane3, text='Dodaj nowe ESP', command = lambda: PaneSwitcher(33, pane3))
        button33.place(relx=0.3, rely=0.8, relwidth=0.4, relheight=0.05)
        
        button34 = tkinter.Button(pane3, text='Włącz/wyłącz urządzenie', command = lambda: esp_switch_power (check_list_select(listbox31,pane3,cds), pane3, [0.1, 0.56, 0.8, 0.03]))
        button34.place(relx=0.3, rely=0.5, relwidth=0.4, relheight=0.05)
        
        button36 = tkinter.Button(pane3, text='Włącz/wyłącz funkcję repeatera', command = lambda: esp_switch_repeater(check_list_select(listbox31,pane3,cds), pane3, [0.1, 0.66, 0.8, 0.03]))
        button36.place(relx=0.3, rely=0.6, relwidth=0.4, relheight=0.05)
        
        button35 = tkinter.Button(pane3, text='Pokaż konfigurację', command = lambda: show_esp_config(text31, check_list_select(listbox31,pane3,cds), pane3))
        button35.place(relx=0.3, rely=0.3, relwidth=0.4, relheight=0.05)
    
    elif number==31:
        pane31 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane31.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        cleaning_label = tkinter.Label(pane31, bg = '#c2b3a8')
        cleaning_label.place(relx=0.1, rely=0.46, relwidth=0.8, relheight=0.03)
        
        label311 = tkinter.Label(pane31, bg = '#c2b3a8', text="Częstotliwość synchronizacji z bazą:")
        label311.place(relx=0.05, rely=0.05, relwidth=0.4, relheight=0.05)
        entry311 = tkinter.Entry(pane31)
        entry311.place(relx=0.50, rely=0.05, relwidth=0.45, relheight=0.05)
        
        label312 = tkinter.Label(pane31, bg = '#c2b3a8', text="Indywidualna nazwa urządzenia:")
        label312.place(relx=0.05, rely=0.15, relwidth=0.4, relheight=0.05)
        entry312 = tkinter.Entry(pane31)
        entry312.place(relx=0.50, rely=0.15, relwidth=0.45, relheight=0.05)
        
        label313 = tkinter.Label(pane31, bg = '#c2b3a8', text="Numer sali:")
        label313.place(relx=0.05, rely=0.25, relwidth=0.4, relheight=0.05)
        entry313 = tkinter.Entry(pane31)
        entry313.place(relx=0.50, rely=0.25, relwidth=0.45, relheight=0.05)
        
        label314 = tkinter.Label(pane31, bg = '#c2b3a8', text="Hasło urządzenia:")
        label314.place(relx=0.05, rely=0.35, relwidth=0.4, relheight=0.05)
        entry314 = tkinter.Entry(pane31, show='*')
        entry314.place(relx=0.50, rely=0.35, relwidth=0.45, relheight=0.05)
        
        button311 = tkinter.Button(pane31, text='Ustaw', command = lambda: update_esp_config(args[0][1], [entry311.get(), entry312.get(), entry313.get(), entry314.get()], pane31, [0.1,0.61,0.8,0.03]))
        button311.place(relx=0.3, rely=0.55, relwidth=0.4, relheight=0.05)
        
        button312 = tkinter.Button(pane31, text="Wróć", command = lambda: PaneSwitcher(3, frame1))
        button312.place(relx=0.7, rely=0.9, relwidth=0.2, relheight=0.05)
        
        text311 = tkinter.Text(pane31, bg = '#c2b3a8')
        text311.place(relx=0.05, rely=0.65, relwidth=0.9, relheight=0.1)
        text311.insert('1.0', "UWAGA: Nieuzupełnione pola nie dokonują zmian, aby coś zmienić należy wpisać jakikolwiek znak w puste pole")
    
    elif number==33:
        #TODO: Nie pozwalac na dodawanie pustych ESP
        pane33 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane33.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        cleaning_label = tkinter.Label(pane33, bg = '#c2b3a8')
        cleaning_label.place(relx=0.1, rely=0.46, relwidth=0.8, relheight=0.1)
        
        label331 = tkinter.Label(pane33, bg = '#c2b3a8', text="Częstotliwość synchronizacji z bazą:")
        label331.place(relx=0.05, rely=0.05, relwidth=0.4, relheight=0.05)
        entry331 = tkinter.Entry(pane33)
        entry331.place(relx=0.50, rely=0.05, relwidth=0.45, relheight=0.05)
        
        label332 = tkinter.Label(pane33, bg = '#c2b3a8', text="Indywidualna nazwa urządzenia:")
        label332.place(relx=0.05, rely=0.15, relwidth=0.4, relheight=0.05)
        entry332 = tkinter.Entry(pane33)
        entry332.place(relx=0.50, rely=0.15, relwidth=0.45, relheight=0.05)
        
        label333 = tkinter.Label(pane33, bg = '#c2b3a8', text="Numer sali:")
        label333.place(relx=0.05, rely=0.25, relwidth=0.4, relheight=0.05)
        entry333 = tkinter.Entry(pane33)
        entry333.place(relx=0.50, rely=0.25, relwidth=0.45, relheight=0.05)
        
        label334 = tkinter.Label(pane33, bg = '#c2b3a8', text="Hasło urządzenia:")
        label334.place(relx=0.05, rely=0.35, relwidth=0.4, relheight=0.05)
        entry334 = tkinter.Entry(pane33, show='*')
        entry334.place(relx=0.50, rely=0.35, relwidth=0.45, relheight=0.05)
        
        button331 = tkinter.Button(pane33, text='Ustaw', command = lambda: add_esp(entry332.get(), [entry331.get(), entry332.get(), entry333.get(), entry334.get()], pane33, [0.1,0.6,0.8,0.05]))
        button331.place(relx=0.3, rely=0.55, relwidth=0.4, relheight=0.05)
        
        button332 = tkinter.Button(pane33, text='Wróć', command = lambda: PaneSwitcher(3, frame1))
        button332.place(relx=0.7, rely=0.9, relwidth=0.2, relheight=0.05)
        
    elif number==4:
        pane4 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane4.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        button41 = tkinter.Button(pane4, text='Dodaj źródło pobierania danych', command = lambda: PaneSwitcher(41, pane4))
        button41.place(relx=0.25, rely=0.1, relwidth=0.5, relheight=0.1)
        button42 = tkinter.Button(pane4, text='Usuń źródło pobieranych danych', command = lambda: PaneSwitcher(42, pane4))
        button42.place(relx=0.25, rely=0.3, relwidth=0.5, relheight=0.1)
        button43 = tkinter.Button(pane4, text='Wyświetl obecne źródła', command = lambda: PaneSwitcher(43, pane4))
        button43.place(relx=0.25, rely=0.5, relwidth=0.5, relheight=0.1)
        
    elif number==41:
        pane41 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane41.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        listbox411 = tkinter.Listbox(pane41, selectmode='single', height='4')
        listbox411.place(relx=0.25, rely=0.1, relwidth=0.5, relheight=0.15)
        listbox411.insert(1, "Arkusze Google")
        listbox411.insert(2, "Linki HTML")
        
        label411 = tkinter.Label(pane41, text='Wprowadź link:', bg = '#c2b3a8', font='48', fg='black')
        label411.place(relx=0.25, rely=0.3, relwidth=0.5, relheight=0.1)
        
        entry411 = tkinter.Entry(pane41)
        entry411.place(relx=0.1, rely=0.4, relwidth=0.8, relheight=0.05)
        
        #coordinates for output label
        cds = [0.1, 0.6, 0.8, 0.05]
        button411 = tkinter.Button(pane41, text='Dodaj', command = lambda: put_source(entry411, check_list_select(listbox411, pane41, cds), pane41, [0.1, 0.6, 0.8, 0.05]))
        button411.place(relx=0.3, rely=0.5, relwidth=0.4, relheight=0.05)
            
    elif number==42:
        pane42 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane42.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        listbox421 = tkinter.Listbox(pane42, selectmode='single', height='4')
        listbox421.place(relx=0.25, rely=0.1, relwidth=0.5, relheight=0.15)
        listbox421.insert(1, "Plany zajęć AGH")
        listbox421.insert(2, "Arkusze Google")
        listbox421.insert(3, "Linki HTML")
        listbox421.insert(4, "Arkusze Excel")
        
        label421 = tkinter.Label(pane42, text='Wprowadź link/ścieżkę do pliku:', bg = '#c2b3a8', font='48', fg='black')
        label421.place(relx=0.25, rely=0.3, relwidth=0.5, relheight=0.1)
        
        entry421 = tkinter.Entry(pane42)
        entry421.place(relx=0.1, rely=0.4, relwidth=0.8, relheight=0.05)
        
        #coordinates for output label
        cds = [0.1, 0.6, 0.8, 0.05]
        button421 = tkinter.Button(pane42, text='Usuń', command = lambda: remove_source(entry421, check_list_select(listbox421, pane42, cds), pane42))
        button421.place(relx=0.3, rely=0.5, relwidth=0.4, relheight=0.05)
            
        text421 = tkinter.Text(pane42, bg = '#c2b3a8', font='48', fg='black')
        text421.place(relx=0.1, rely=0.7, relwidth=0.8, relheight=0.2)
        text421.insert('1.0', 'UWAGA: Jeśli w bazie danych jest więcej niż jeden wpis zgadzający się z podanym wyżej, program usunie je wszystkie!')
        
    elif number==43:
        pane43 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane43.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        listbox431 = tkinter.Listbox(pane43, selectmode='single', height='4')
        listbox431.place(relx=0.25, rely=0.01, relwidth=0.5, relheight=0.15)
        listbox431.insert(1, "Plany zajęć AGH")
        listbox431.insert(2, "Arkusze Google")
        listbox431.insert(3, "Linki HTML")
        listbox431.insert(4, "Arkusze Excel")
        
        text431 = tkinter.Text(pane43)
        text431.place(relx=0.1, rely=0.3, relwidth=0.8, relheight=0.6)
        
        #coordinates for output label
        cds = [0.1, 0.26, 0.8, 0.03]
        button431 = tkinter.Button(pane43, text='Szukaj', command = lambda: read_sources(check_list_select(listbox431, pane43, cds), text431))
        button431.place(relx=0.3, rely=0.2, relwidth=0.4, relheight=0.05)
        
    elif number==5:
        pane5 = tkinter.PanedWindow(frame,bg = '#c2b3a8')
        pane5.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        try:
            aps = read_aps()
            len_aps = len(aps)
        except:
            aps = ["No APs in database"]
        
        listbox51 = tkinter.Listbox(pane5, selectmode='single', height=str(0))
        listbox51.place(relx=0.1, rely=0.05, relwidth=0.2, relheight=0.2)
        for index, ap in enumerate(aps):
            listbox51.insert(index+1, str(ap))
        
        text51 = tkinter.Text(pane5)
        text51.place(relx=0.4, rely=0.05, relwidth = 0.5, relheight=0.2)
        
        cds = [0.1, 0.26, 0.8, 0.03]
        button51 = tkinter.Button(pane5, text='Ustaw konfigurację wybranego AP', command = lambda: PaneSwitcher(51, pane5, check_list_select(listbox51, pane5, cds)))
        button51.place(relx=0.3, rely=0.4, relwidth=0.4, relheight=0.05)
        
        button52 = tkinter.Button(pane5, text='Usuń AP z bazy danych', command = lambda: remove_ap(check_list_select(listbox51,pane5,cds), pane5, [0.1, 0.76, 0.8, 0.03]))
        button52.place(relx=0.3, rely=0.5, relwidth=0.4, relheight=0.05)
        
        button53 = tkinter.Button(pane5, text='Dodaj nowe AP', command = lambda: PaneSwitcher(53, pane5))
        button53.place(relx=0.3, rely=0.6, relwidth=0.4, relheight=0.05)
        
        button54 = tkinter.Button(pane5, text='Pokaż konfigurację wybranego AP', command = lambda: show_aps(text51, check_list_select(listbox51,pane5,cds), pane5))
        button54.place(relx=0.3, rely=0.3, relwidth=0.4, relheight=0.05)
        
    elif number==51:
        pane51 = tkinter.PanedWindow(frame,bg = '#c2b3a8')
        pane51.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        cleaning_label = tkinter.Label(pane51, bg = '#c2b3a8')
        cleaning_label.place(relx=0.1, rely=0.46, relwidth=0.8, relheight=0.03)
        
        label511 = tkinter.Label(pane51, bg = '#c2b3a8', text="Indywidualna nazwa urządzenia:")
        label511.place(relx=0.05, rely=0.15, relwidth=0.4, relheight=0.05)
        entry511 = tkinter.Entry(pane51)
        entry511.place(relx=0.50, rely=0.15, relwidth=0.45, relheight=0.05)
        
        label512 = tkinter.Label(pane51, bg = '#c2b3a8', text="Hasło urządzenia:")
        label512.place(relx=0.05, rely=0.35, relwidth=0.4, relheight=0.05)
        entry512 = tkinter.Entry(pane51, show='*')
        entry512.place(relx=0.50, rely=0.35, relwidth=0.45, relheight=0.05)
        
        button511 = tkinter.Button(pane51, text='Ustaw', command = lambda: update_ap_config(args[0][1], [entry511.get(), entry512.get()], pane51, [0.1,0.61,0.8,0.03]))
        button511.place(relx=0.3, rely=0.55, relwidth=0.4, relheight=0.05)
        
        button512 = tkinter.Button(pane51, text="Wróć", command = lambda: PaneSwitcher(5, frame1))
        button512.place(relx=0.7, rely=0.9, relwidth=0.2, relheight=0.05)
        
        text511 = tkinter.Text(pane51, bg = '#c2b3a8')
        text511.place(relx=0.05, rely=0.65, relwidth=0.9, relheight=0.1)
        text511.insert('1.0', "UWAGA: Nieuzupełnione pola nie dokonują zmian, aby coś zmienić należy wpisać jakikolwiek znak w puste pole")
    
    elif number==53:
        #TODO: Nie pozwalac na dodawanie pustych AP
        pane53 = tkinter.PanedWindow(frame, bg = '#c2b3a8')
        pane53.place(relx=0, rely=0, relwidth=1, relheight=1)
        
        cleaning_label = tkinter.Label(pane53, bg = '#c2b3a8')
        cleaning_label.place(relx=0.1, rely=0.46, relwidth=0.8, relheight=0.1)
        
        label531 = tkinter.Label(pane53, bg = '#c2b3a8', text="Nazwa AP:")
        label531.place(relx=0.05, rely=0.15, relwidth=0.4, relheight=0.05)
        entry531 = tkinter.Entry(pane53)
        entry531.place(relx=0.50, rely=0.15, relwidth=0.45, relheight=0.05)
        
        label532 = tkinter.Label(pane53, bg = '#c2b3a8', text="Hasło AP:")
        label532.place(relx=0.05, rely=0.35, relwidth=0.4, relheight=0.05)
        entry532 = tkinter.Entry(pane53, show='*')
        entry532.place(relx=0.50, rely=0.35, relwidth=0.45, relheight=0.05)
        
        button531 = tkinter.Button(pane53, text='Ustaw', command = lambda: add_ap(entry531.get(), [entry531.get(), entry532.get()], pane53, [0.1,0.6,0.8,0.05]))
        button531.place(relx=0.3, rely=0.55, relwidth=0.4, relheight=0.05)
        
        button532 = tkinter.Button(pane53, text='Wróć', command = lambda: PaneSwitcher(5, frame1))
        button532.place(relx=0.7, rely=0.9, relwidth=0.2, relheight=0.05)
        
    else:
        return 0
    
db = connect_db()   
main_window = tkinter.Tk()


main_window.title('Tabliczki panel zarządzania')

tkinter.Canvas(main_window,height=600, width=1000).pack()

frame0 = tkinter.Frame(main_window, bg = '#8e8582')
frame0.place(relx=0, rely=0, relwidth=0.3, relheight=1)

frame1 = tkinter.Frame(main_window, bg = '#c2b3a8')
frame1.place(relx=0.3, rely=0, relwidth=0.7, relheight=1)

button1 = tkinter.Button(frame0, text='Zarządzaj jednostką centralną', command = lambda: PaneSwitcher(1, frame1))
button1.place(bordermode = 'outside', relheight=0.05, relwidth=0.8, relx=0.1, rely=0.05)

button2 = tkinter.Button(frame0, text='Połączenie przez port szeregowy', command = lambda: PaneSwitcher(2,frame1))
button2.place(bordermode = 'outside', relheight=0.05, relwidth=0.8, relx=0.1, rely=0.15)

button3 = tkinter.Button(frame0, text='Zarządzanie ESP', command = lambda: PaneSwitcher(3,frame1))
button3.place(bordermode = 'outside', relheight=0.05, relwidth=0.8, relx=0.1, rely=0.25)

button4 = tkinter.Button(frame0, text='Zarządzanie źródłami danych', command = lambda: PaneSwitcher(4,frame1))
button4.place(bordermode = 'outside', relheight=0.05, relwidth=0.8, relx=0.1, rely=0.35)

button5 = tkinter.Button(frame0, text="Konfiguruj Access Point", command = lambda: PaneSwitcher(5, frame1))
button5.place(bordermode = 'outside', relheight=0.05, relwidth=0.8, relx=0.1, rely=0.45)

main_window.mainloop()
