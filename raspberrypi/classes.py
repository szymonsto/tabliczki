
        
class Teacher:
    def __init__ (self, name, room, office_hours, weekdays, mails, update_date):
        self.name = name
        self.room = room
        self.office_hours = office_hours
        self.weekdays = weekdays
        self.mails = mails
        self.update_date = update_date
    def show_object(self):
        print("Teacher name: " + self.name)
        print("Mail: " + self.mails)
        print("Room number: " + self.room)
        for i in range(len(self.weekdays)):
            print("When: ", self.office_hours[i], self.weekdays[i])
        print("Downloaded at: " + self.update_date[0], self.update_date[1], "\n")
        print("\n")
    def return_obj(self):
        obj = {
            "name": self.name,
            "room": self.room,
            "office_hours": self.office_hours,
            "weekdays": self.weekdays,
            "mails": self.mails,
            "update_date": self.update_date
            }
        return obj
    def return_type(self):
        return type(self)
    
        
class Classes:
    def __init__ (self, teacher_names, classes_names, room, hours, weekday, mails, update_date):
        self.teacher_names = teacher_names
        self.classes_names = classes_names
        self.room = room
        self.hours = hours
        self.weekday = weekday
        self.mails = mails
        self.update_date = update_date
        
    def show_object(self):
        if type(self.teacher_names) == list:
            for name in self.teacher_names:
                print("Teacher name: " + name)
        else:
            print("Teacher name: " + self.teacher_names)
        print("Subject: " + self.classes_names)
        print("Room number: " + self.room)
        print("When: ", self.hours[0], " - ", self.hours[1], ", ", self.weekday)
        print("Downloaded at: " + self.update_date[0], self.update_date[1], "\n")
    
    def return_obj(self):
        obj = {
            "t_names": self.teacher_names,
            "c_names": self.classes_names,
            "room": self.room,
            "hours": self.hours,
            "weekday": self.weekday,
            "mails": self.mails,
            "update_date": self.update_date
            }
        return obj
    def return_type(self):
        return type(self)
        
#Class that creates objects with parameters: url address and status - that says if given url has been already checked (without that checking these urls is never ending story)
class URL_link:
    def __init__(self, address, status):
        self.address = address
        self.status = status
    
    def change_status (self, status):
        self.status = status
        
    def show_object (self):
        print("URL: ", self.address)