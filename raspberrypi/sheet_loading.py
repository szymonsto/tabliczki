import pandas as pd
import classes
from datetime import datetime
import logging

### link: https://www.youtube.com/watch?v=t6WSY9D_ORQ

def google_sheet_reading(link):
    
    base_link = link
    
    #removes http://www. or https://www. from a base_link for example: http://www.iet.agh.edu.pl --> iet.agh.edu.pl, http://home.agh.edu.pl --> home.agh.edu.pl
    #or adds https:// if there is none in a base_link
#     print("Podany argument: ", base_link)
    if base_link.find("https://") >= 0 or base_link.find("http://") >= 0:
        pass
    else:
        base_link = "https://"+base_link
    
    #removing part of a link that is raising errors. If there is none then find method returns -1 which assigns the whole string again
    base_link = base_link[:base_link.find("/edit")]
    
    #extracting sheetID from a link
    sheetID = base_link[base_link.find("/d/")+3:]
    
    postfix = "/export?format=csv"
    
#     print("Link po edycjach: ", base_link+postfix)
    
    try:
        data = pd.read_csv(base_link+postfix)
        RoomTypes = data['Room Type'].values
        RoomNumbers = data['Room Number'].values
        Names = data['Name'].values
        Dates1 = data['Hours'].values
        Dates2 = data['Weekday'].values
        Email = data['E-mail'].values
        Classes = data['Class'].values
        ModDates = data['Last Modified'].values
    except:
        logging.error("The data available in the worksheet %s does not match the template. Must contain columns named: Room Type, Room Number, Name, Hours, Weekday, E-mail, Class. The order is not important", baselink+postfix)
        return 0
        
    Objects = []

    for i in range (len(Names)):
        
        Dates1[i] = Dates1[i].split(';')
        ModDates[i] = ModDates[i].split()
        if RoomTypes[i] == "Teacher":
            Dates2[i] = Dates2[i].split(';')
        
        if RoomTypes[i] == "Teacher":
            obj = classes.Teacher (Names[i], RoomNumbers[i], Dates1[i], Dates2[i], Email[i], ModDates[i])
            Objects.append(obj)
        elif RoomTypes[i] == "Classes":
            Dates1[i] = Dates1[i][0].split('-')
            obj = classes.Classes (Names[i], Classes[i], RoomNumbers[i], Dates1[i], Dates2[i], Email[i], ModDates[i])
            Objects.append(obj)
    return Objects

#testing
z = google_sheet_reading("https://docs.google.com/spreadsheets/d/1IhKkZDihBMenMXzbNIbWh7Lehi8MFiCcUgAq9sL87Pk/edit#gid=0")
z = google_sheet_reading("http://docs.google.com/spreadsheets/d/1IhKkZDihBMenMXzbNIbWh7Lehi8MFiCcUgAq9sL87Pk/edit#gid=0")
z = google_sheet_reading("docs.google.com/spreadsheets/d/1IhKkZDihBMenMXzbNIbWh7Lehi8MFiCcUgAq9sL87Pk/edit#gid=0")