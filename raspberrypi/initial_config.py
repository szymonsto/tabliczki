import os
import platform

print("Tworzenie odpowiednich folderów w katalogu /home/pi/ oraz kopiowanie zawartości programów do nich")
os.system('cp -r doorlabels /home/pi/')
os.system('cp -r doorlabels_sources /home/pi/')
print("Tworzenie folderów i kopiowanie zakończone pomyślnie.")

print("Tworzenie usługi wificonnect odpowiedzialnej za automatyczne łączenie z najbliższą sieci WiFi")
os.system('cat doorlabels/wificonnect.txt > wificonnect.service')
os.system('sudo cp wificonnect.service /etc/systemd/system/')
os.system('rm wificonnect.service')
os.system('sudo systemctl daemon-reload')
os.system('sudo systemctl enable wificonnect.service')
print("Tworzenie zakończono pomyślnie")

print("Tworzenie usługi getip odpowiedzialnej za wpis IP do bazy danych")
os.system('cat doorlabels/getip.txt > getip.service')
os.system('sudo cp getip.service /etc/systemd/system/')
os.system('rm getip.service')
os.system('sudo systemctl daemon-reload')
os.system('sudo systemctl enable getip.service')
print("Tworzenie zakończono pomyślnie")

print("Tworzenie usługi run_doorlabels odpowiedzialnej za systematyczne wykonywanie programu akwizycji danych")
os.system('cat doorlabels/serconfig.txt > run_doorlabels.service')
os.system('sudo cp run_doorlabels.service /etc/systemd/system/')
os.system('rm run_doorlabels.service')
os.system('sudo systemctl daemon-reload')
os.system('sudo systemctl enable run_doorlabels.service')
print("Tworzenie zakończono pomyślnie")


if platform.system()=="Windows":
    os.system("pip install pandas")
else:
    os.system("pip3 install pandas")
    
if platform.system()=="Windows":
    os.system("pip install pyrebase")
else:
    os.system("pip3 install pyrebase")
    
if platform.system()=="Windows":
    os.system("pip install beautifulsoup4")
else:
    os.system("pip3 install beautifulsoup4")
    
if platform.system()=="Windows":
    os.system("pip install datetime")
else:
    os.system("pip3 install datetime")
    
if platform.system()=="Windows":
    os.system("pip install pyserial")
else:
    os.system("pip3 install pyserial")

if platform.system()=="Windows":
    os.system("pip install futures3")
else:
    os.system("pip3 install futures")
    
if platform.system()=="Windows":
    os.system("pip install tzlocal")
else:
    os.system("pip3 install tzlocal")
    
if platform.system()=="Windows":
    os.system("pip install pywifi")
else:
    os.system("pip3 install pywifi")
    
if platform.system()=="Windows":
    os.system("pip install wifi")
else:
    os.system("pip3 install wifi")

if platform.system()=="Windows":
    os.system("pip install xlrd")
else:
    os.system("pip3 install xlrd")