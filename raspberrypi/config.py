#! /usr/bin/env python3
import firebase_init
import socket
import subprocess

#na ten moment musi wystarczyć
#Kod poniżej rozumiem tak: tworzymy gniazdo, inicjujemy połączenie z dowolnym adresem (musi być osiągalny) i na tej podstawie uzyskujemy dane o src ip

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip = s.getsockname()[0]
ssid = subprocess.check_output('iw wlan0 link | grep SSID', stderr=subprocess.STDOUT, shell=True)
ssid = str(ssid)
ssid = ssid[ssid.find('SSID:')+5:]
ssid = ssid[:-3]
s.close()
firebase_init.write_ip(ip, ssid)