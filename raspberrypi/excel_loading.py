import pandas as pd
import classes
from datetime import datetime
import os
import logging


def excel_reading(path):

    try:
        info = pd.ExcelFile(path)
    except FileNotFoundError:
        logging.error("%s: Given file does not exists or is of wrong file extension!", path)
        return 0
    except:
        logging.error("While opening %s an error has occured", path)
        return 0
    
    info_sheets = []
    for sheet in info.sheet_names:
       info_sheets.append(info.parse(sheet))
    data = pd.concat(info_sheets)

    RoomTypes = data['Room Type'].values
    RoomNumbers = data['Room Number'].values
    Names = data['Name'].values
    Dates1 = data['Hours'].values
    Dates2 = data['Weekday'].values
    Email = data['E-mail'].values
    Classes = data['Class'].values
    
    Objects = []

    for i in range (len(Names)):
        Dates1[i] = Dates1[i].split(';')
        if RoomTypes[i] == "Teacher":
            Dates2[i] = Dates2[i].split(';')
           
        when = os.path.getmtime(path)
        when = datetime.fromtimestamp(when)
        when = str(when)[:-7]
        when = when.split()
        if RoomTypes[i] == "Teacher":
            obj = classes.Teacher (Names[i], RoomNumbers[i], Dates1[i], Dates2[i], Email[i], when)
            Objects.append(obj)
        elif RoomTypes[i] == "Classes":
            Dates1[i] = Dates1[i][0].split('-')
            obj = classes.Classes (Names[i], Classes[i], RoomNumbers[i], Dates1[i], Dates2[i], Email[i], when)
            Objects.append(obj)
        
    return Objects

excel_reading("/home/pi/Desktop/tabliczki_sources/test.xls")