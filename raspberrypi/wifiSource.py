from wifi import Cell        
import pywifi
from pywifi import const

wifi = pywifi.PyWiFi()
iface = wifi.interfaces()[-1]

def read_profiles():
    profiles = iface.network_profiles()
    return profiles

def read_profile(ssid):
    profiles = read_profiles()
    for profile in profiles:
        if profile.ssid == ssid:
            return profile
    return 0
    
def create_profile (ssid, key):
    profile = pywifi.Profile()
    profile.ssid = ssid
    profile.auth = const.AUTH_ALG_OPEN
    profile.akm.append(const.AKM_TYPE_WPA2PSK)
    profile.cipher = const.CIPHER_TYPE_CCMP
    profile.key = key
    if not read_profile(profile.ssid):
        iface.add_network_profile(profile)
    return profile

def remove_profile(profile):
    iface.remove_network_profile(profile)
    
def get_matches ():
    cells = Cell.all(iface.name())
    cells1 = []
    for cell in cells:
        cells1.append(cell.ssid)
    networks = read_profiles()
    networks1 = []
    for network in networks:
        networks1.append(network.ssid)
    matches = set(cells1) & set(networks1)
    profiles = []
    for match in matches:
        profiles.append(read_profile(match))
    return profiles
    
def connect_auto(profile):
    try:
        iface.connect(profile)
        count = 0
        while iface.status()!=4:
            count += 1
            if count>200000:
                print("Nie udalo sie polaczyc z siecia. Czy ona na pewno istnieje?")
                return 0
        return 1
    except:
        return 0
    
def connect():
    if not iface.status()==4 and not iface.status()==3:
        profiles = get_matches()
        if profiles:
            for profile in profiles:
                if connect_auto(profile):
                    return 1
            print("Cannot connect with any network")
        else:
            print("There are no known networks in the area")