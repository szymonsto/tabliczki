import pandas as pd
import classes
from datetime import datetime
import os
import logging
import xlrd
from webcolors import rgb_to_name

def get_color(file, sheet, row, col):
    sheet = file.sheet_by_name(sheet.name)
    cell = sheet.cell_xf_index(row, col)
    xf = file.xf_list[cell]
    bgx = xf.background.pattern_colour_index
    pattern_colour = file.colour_map[bgx]
    if pattern_colour == None:
        pattern_colour = (255,255,255)
    return pattern_colour

def compare_colors (c1, c2):
    result=0
    for i in range (3):
        if c1[i]==c2[i]:
            result = result + 1
    if result==3:
        return 1
    else:
        return 0        
    
def find_name (string):
    temp_borders = []
    if str(string).find('(') >= 0 and str(string).find(')') >= 0:
        temp_borders.append(str(string).find('('))
        temp_borders.append(str(string).find(')'))
        name = string[temp_borders[0]+1:temp_borders[1]]
        return name
    else:
        return ""

def find_subject (string):
    if str(string).find('(') >= 0:
        subject = string[:str(string).find('(')-3]
        return subject
    else:
        return string

#for index, sheet in enumerate(info_sheets):
def organizing_data(classes_list, sheet, when, path, file):
    
    #Dividing sheet by columns. Each column is now separate list and is being added to
    #a group list named columns_list
    columns_list = []
    for column in range (62):
        temp = sheet
        columns_list.append(temp[str(column+1)].tolist())


    #At the moment (October 2020) file contains current and past schedules.
    #For loop below aims to remove past schedules from lists
    for ind in range (len(columns_list)):
        for ind1 in range (69, len(columns_list[ind])):
            columns_list[ind].pop(69)
        
    #Given in a file hours does not match. Example:
    #Room: 1-D5, at 8am Mondays, 2020/21 Winter, there is "Probabilistyka i statystyka" led by dr. Swatowska.
    #Classes name is on B8 cell, while 8am on A4 cell, thus start time is a fourth element of a list and classes name eigth.
    #For loop below aims to make start-time eigth element of a list as well (in this particular example)
    #It is not necessary, but will be easier to compare and not get mistaken
    base_first_column = []
    for slot in range (len(columns_list[0])):
        if columns_list[0][slot] != 'nan':
            base_first_column.append(columns_list[0][slot])
            columns_list[0][slot] = columns_list[0][slot] - 1


    #Next if checks if given data is up to date by comparing current and inside file's dates.
    #If so then program continues. If not, skips sheet.
    now = datetime.now()
    day_date = now.strftime("%D")
    year_date = "20"+str(day_date[6:])
    month_date = str(day_date[:2])
    
    #if (x&&y) or (z&&y)
    if (str(columns_list[1][0]).find("ZIMOWY") >= 0 and int(month_date) >= 10 and str(columns_list[1][0]).find(year_date) >= 0) or (int(month_date) >= 2 and int(month_date) < 10 and str(columns_list[1][0]).find("LETNI") >= 0 and str(columns_list[1][0]).find(year_date) >= 0):
        
        #Breaks in schedule named:
        base_breaks = ['9:30 - 9:35',
                       '11:05 - 11:15',
                       '12:45 - 12:50',
                       '14:20 - 14:40',
                       '16:10 - 16:15',
                       '17:45 - 17:50',
                       '19:20 - 19:25']
        
        #Breaks read from sheet
        breaks = {
            13: 5,
            20: 10,
            27: 5,
            34: 20,     #Przerwa dwudziestominutowa zawiera sie w 3 rzedach excela
            35: 0,
            36: 0,
            43: 5,
            50: 5,
            57: 5
        }    
        #For running through column's values.
        #If Nan - skips, if data -> managing:
        #Separates and organizes data into variables and creates object
        interesting_columns = [1, 13, 25, 37, 49]
        for number in interesting_columns:
            for ind, row in enumerate(columns_list[number]):
                day = columns_list[number][2]
                if str(row) != "nan" and str(row).find("PRZERWA") < 0 and ind > 2:
                    subject = find_subject(row)
                    name = find_name(row)
                    hours = []
                    if str(columns_list[0][ind]) != "nan":
                        hours.append(str(int(columns_list[0][ind])) + ':00')
                        if str(columns_list[number][ind + 6]).find("PRZERWA") >= 0:
                            temp_string = str(columns_list[number][ind + 6])
                            hours.append(temp_string[8:13])
                        else:
                            cells = 0
                            cell_color = get_color(file, sheet, ind, number)
                            temp_row = ind
                            count = 0
                            break_time = 0
                            while 1:
                                temp_row += 1
                                if not compare_colors(cell_color, get_color(file, sheet, temp_row, number)) or str(columns_list[number][temp_row]) != "nan" or temp_row>=65:
                                    break
                                if temp_row not in breaks.keys():
                                    count += 1
                                else:
                                    break_time = break_time + breaks[temp_row]
                                
                            hour = int(count / 4)
                            hour = str(int(columns_list[0][ind])+hour)
                            minutes = (count % 4) * 15 + break_time
                            if minutes == 0:
                                minutes = str(minutes)+"0"
                            elif minutes>=60:
                                h = int(minutes / 60)
                                hour = str(int(hour) + h)
                                minutes = str(int(minutes % 60))
                                if int(minutes)<10:
                                    minutes = "0"+str(minutes)
                            else:
                                minutes = str(minutes)
                            hours.append(hour+":"+minutes)
                    elif str(columns_list[number][ind - 1]).find("PRZERWA") >= 0:
                        temp_string = str(columns_list[number][ind - 1])
                        hours.append(temp_string[-5:])
                        if str(columns_list[number][ind + 6]).find("PRZERWA") >= 0:
                            temp_string = str(columns_list[number][ind + 6])
                            hours.append(temp_string[8:13])
                        else:
                            cells = 0
                            cell_color = get_color(file, sheet, ind, number)
                            temp_row = ind
                            count=1
                            break_time = 0
                            while 1:
                                temp_row += 1
                                if not compare_colors(cell_color, get_color(file, sheet, temp_row, number)) or str(columns_list[number][temp_row]) != "nan" or temp_row>=65:
                                    break
                                if temp_row not in breaks.keys():
                                    count += 1
                                else:
                                    break_time = break_time + breaks[temp_row]
                                
                            if temp_string[-5:][:2].find(":")>=0:
                                hour = int(temp_string[-5:][:1])
                            else:
                                hour = int(temp_string[-5:][:2])
                            hour = str(hour + int(count / 4))
                            minutes = (count % 4) * 15 + int(temp_string[-2:]) + break_time
                            if minutes == 0:
                                minutes = str(minutes)+"0"
                            elif minutes>=60:
                                h = int(minutes / 60)
                                hour = str(int(hour) + h)
                                minutes = str(int(minutes % 60))
                                if int(minutes)<10:
                                    minutes = "0"+str(minutes)
                            else:
                                minutes = str(minutes)
                            hours.append(hour+":"+minutes)                            
                    elif str(columns_list[number][ind - 3]).find("PRZERWA") >= 0:
                        temp_string = str(columns_list[number][ind - 3])
                        hours.append(temp_string[-5:])
                        if str(columns_list[number][ind + 6]).find("PRZERWA") >= 0:
                            temp_string = str(columns_list[number][ind + 6])
                            hours.append(temp_string[8:13])
                        else:
                            cells = 0
                            cell_color = get_color(file, sheet, ind, number)
                            temp_row = ind
                            count=1
                            break_time = 0
                            while 1:
                                temp_row += 1
                                if not compare_colors(cell_color, get_color(file, sheet, temp_row, number)) or str(columns_list[number][temp_row]) != "nan" or temp_row>=65:
                                    break
                                if temp_row not in breaks.keys():
                                    count += 1
                                else:
                                    break_time = break_time + breaks[temp_row]
                                
                            if temp_string[-5:][:2].find(":")>=0:
                                hour = int(temp_string[-5:][:1])
                            else:
                                hour = int(temp_string[-5:][:2])
                            hour = str(hour + int(count / 4))
                            minutes = (count % 4) * 15 + int(temp_string[-2:]) + break_time
                            if minutes == 0:
                                minutes = str(minutes)+"0"
                            elif minutes>=60:
                                h = int(minutes / 60)
                                hour = str(int(hour) + h)
                                minutes = str(int(minutes % 60))
                                if int(minutes)<10:
                                    minutes = "0"+str(minutes)
                            else:
                                minutes = str(minutes)
                            hours.append(hour+":"+minutes)      
                    else:
                        #columns_list[0][7] is 8am, and every single next element of the list is another 15 minutes
                        count = 0
                        time = ""
                        for cell in range (7, ind+1):
                            if str(columns_list[0][cell]) == 'nan':
                                if cell in breaks:
                                    pass
                                else:
                                    count+=1
                            else:
                                count = 0
                                time = columns_list[0][cell]
                        if time == "":
                            for x in range (3,7):
                                if str(columns_list[number][x]) != "nan":
                                    count = x-3
                                    minutes = count*15
                                    if minutes == 0:
                                        minutes = "00"
                                    start_time = "7:" + str(minutes)
                                    hours.append(start_time)
                        else:
                            minutes = count*15
                            if minutes == 0:
                                minutes = "00"
                            start_time = str(time)[:-2] + ":" + str(minutes)
                            hours.append(start_time)
                        if str(columns_list[number][ind + 6]).find("PRZERWA") >= 0:
                            temp_string = str(columns_list[number][ind + 6])
                            hours.append(temp_string[8:13])
                        else:
                            cells = 0
                            cell_color = get_color(file, sheet, ind, number)
                            temp_row = ind
                            count=1
                            break_time = 0
                            while 1:
                                temp_row += 1
                                if not compare_colors(cell_color, get_color(file, sheet, temp_row, number)) or str(columns_list[number][temp_row]) != "nan" or temp_row>=65:
                                    break
                                if temp_row not in breaks.keys():
                                    count += 1
                                else:
                                    break_time = break_time + breaks[temp_row]
                                
                            if start_time[-5:][:2].find(":")>=0:
                                hour = int(start_time[-5:][:1])
                            else:
                                hour = int(start_time[-5:][:2])
                            hour = str(hour + int(count / 4))
                            minutes = (count % 4) * 15 + int(start_time[-2:]) + break_time
                            if minutes == 0:
                                minutes = str(minutes)+"0"
                            elif minutes>=60:
                                h = int(minutes / 60)
                                hour = str(int(hour) + h)
                                minutes = str(int(minutes % 60))
                                if int(minutes)<10:
                                    minutes = "0"+str(minutes)
                            else:
                                minutes = str(minutes)
                            hours.append(hour+":"+minutes)
                        
                        
                    obj = classes.Classes(name, subject, columns_list[49][0], hours, day, "", when)
                    classes_list.append(obj)
                    #obj.show_object()
    else:
        logging.error("%s in file %s does not contain up to date data! This sheet has been omitted", sheet.name, path)

#TODO: zrobic funkcje z poszczególnych ifów bo idzie pierdolca dostac
#TODO2: ogranąć niepokorne zajecia które nie zaczynają sie równo z końcem przerwy: start time done, został jeszcze end time

#main function in this file, uses all above directly or indirectly, returns list of Classes objects (from classes.py)
def read_schedule (path):
    

    #creating titles for every each one of column. The name is equal to its number so
    #first column is named "1", second "2" etc
    #All of this is based on current (2020) schedule scheme by Zbigniew Korzeń.
    column_names = []
    classes_list = []
    for index in range (62):
        column_names.append(str(index+1))
        
    try:
        file = xlrd.open_workbook(path, formatting_info=True)
        info = pd.ExcelFile(file)
    except FileNotFoundError:
        logging.error("%s: Given file does not exists or is of wrong file extension - agh_excel.py!", path)
        return 0
    except:
        logging.error("While opening %s an error has occured", path)
        return 0

    #Suppose you are reading dataframes in a loop. With ExcelFile.parse you just pass the Excelfile object(xl in your case).
    #So the excel sheet is just loaded once and you use this to get your dataframes. In case of Read_Excel you pass the path instead of Excelfile object.
    #So essentially every time the workbook is loaded again. Makes a mess if your workbook has loads of sheets and tens of thousands of rows.

    #Put every sheet as a separate pd.read_excel's object into info_sheets list
    info_sheets = []
    for name in info.sheet_names:
        sheet = pd.read_excel(info, name, names = column_names, usecols="A:BJ", index_col=None, header=None)
        sheet.name = name
        info_sheets.append(sheet)
     
    when = os.path.getmtime(path)
    when = datetime.fromtimestamp(when)
    when = str(when).split()
    
    for index, sheet in enumerate(info_sheets):
        try:
            organizing_data(classes_list, sheet, when, path, file)
            logging.info("Sheet %s read successfully", info.sheet_names[index])
        except:
            logging.error("Sheet %s in file %s broke program and will be omitted", info.sheet_names[index], path)
    return classes_list