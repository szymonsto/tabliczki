import pyrebase
from datetime import datetime
import classes
import logging

configuration = {
    "apiKey": "AIzaSyBPKwn6GHk65vZbHDJm04QZhLDtOO4uemA",
    "authDomain": "tabliczki-dfd63.firebaseapp.com",
    "databaseURL": "https://tabliczki-dfd63.firebaseio.com",
    "projectId": "tabliczki-dfd63",
    "storageBucket": "tabliczki-dfd63.appspot.com",
    "messagingSenderId": "1037582045549",
    "appId": "1:1037582045549:web:79ab799bc2e617eb53ca64"
}

daynames = {
    "poniedziałek": "Monday",
    "wtorek": "Tuesday",
    "środa": "Wednesday",
    "czwartek": "Thursday",
    "piątek": "Friday"
}


firebase = pyrebase.initialize_app(configuration)

db = firebase.database()

def find_a_dot (string):
    if string.find('.')>=0:
        string1 = string[string.find('.')+1:] 
        string2 = string[:string.find('.')]

        string = string2 + ':' + string1

    return string

def find_a_colon (string):
    if string.find(':')>=0:
        string1 = string[string.find(':')+1:] 
        string2 = string[:string.find(':')]

        string = string2 + '.' + string1

    return string

def add_room (obj):
    try:
        inputs = obj.return_obj()
        inputs['room'] = find_a_dot(inputs['room'])
        db.child("Room").child(inputs["room"]).child(daynames[inputs["weekday"].lower()]).child(inputs["hours"][0]).set(inputs)
    except:
        logging.error("Connection with database cannot be established while adding classroom %s", obj.room)
        return 0
        
def add_teacher (obj):
    try:
        inputs = obj.return_obj()
        if inputs["name"]:
            inputs['name'] = find_a_dot(inputs['name'])
            db.child("Teacher").child(inputs["name"]).set(inputs)
        else:
            db.child("Teacher").child("NoName").set(inputs)
    except:
        logging.error("Connection with database cannot be established while adding teacher %s", obj.name)
        return 0


def update_room (obj):
    try:
        inputs = obj.return_obj()
        if inputs["room"]:
            inputs['room'] = find_a_dot(inputs['room'])
            db.child("Room").child(inputs["room"]).child(daynames[inputs["weekday"].lower()]).child(inputs["hours"][0]).update(inputs)
        else:
            db.child("Room").child("NoName").update(inputs)
    except:
        logging.error("Connection with database cannot be established while updating classroom %s", obj.room)
        return 0
        
def update_teacher (obj):
    try:
        inputs = obj.return_obj()
        if inputs["name"]:
            inputs['name'] = find_a_dot(inputs['name'])
            db.child("Teacher").child(inputs["name"]).update(inputs)
        else:
            db.child("Teacher").child("NoName").update(inputs)
    except:
        logging.error("Connection with database cannot be established while updating teacher %s", obj.name)
        return 0
        
def read_room (number, day, hour):
    try:
        info = db.child("Room").child(str(find_a_dot(number))).child(daynames[str(day).lower()]).child(str(hour)).get()
        keys = []
        vals = []
        for k, v in info.val().items():
            keys.append(k)
            vals.append(v)
        obj = classes.Classes(vals[4], vals[0], find_a_colon(vals[3]), vals[1], vals[6], vals[2], vals[5])
        #obj.show_object()
        return obj
    except:
        logging.error("Connection with database cannot be established while getting classes %s on %s from firebase", number, day)
        return 0
    
def read_teacher (name):
    try:
        info = db.child("Teacher").child(str(find_a_dot(name))).get()
        keys = []
        vals = []
        for k, v in info.val().items():
            keys.append(k)
            vals.append(v)
        obj = classes.Teacher(find_a_colon(vals[1]), vals[3], vals[2], vals[5], vals[0], vals[4])
        #obj.show_object()
        return obj
    except:
        logging.error("Connection with database cannot be established while getting teacher %s from firebase", name)
        return 0

def write_ip (ip_address, ssid):
    try:
        db.child("Rasp Pi").child("IP").set(str(ip_address))
        db.child("Rasp Pi").child("SSID").set(str(ssid))
    except:
        logging.error("Connection with database cannot be established: WR_IP")
        return 0
def read_ip():
    try:
        ip = db.child("Rasp Pi").child("IP").get()
        return ip.val()
    except:
        logging.error("Connection with database cannot be established or asked data does not exist: Read_IP")
        return 0


def add_source(source, s_type):
    try:
        if str(s_type) == "html":
            db.child("Sources").child("HTML Links").push(str(source))
        elif str(s_type) == "excel":
            db.child("Sources").child("MS Excel Files").push(str(source))
        elif str(s_type) == "sheet":
            db.child("Sources").child("Google Sheets").push(str(source))
        elif str(s_type) == "schedule":
            db.child("Sources").child("AGH Schedules").push(str(source))
        else:
            logging.error("firebase_init.add_source(): Source type: %s is unknown", s_type)
    except:
        logging.error("Adding source %s, %s has failed", source, s_type)
        return 0
    
def read_source(s_type):
    try:
        if str(s_type) == "html":
            sources = db.child("Sources").child("HTML Links").get().val().items()
        elif str(s_type) == "excel":
            sources = db.child("Sources").child("MS Excel Files").get().val().items()
        elif str(s_type) == "sheet":
            sources = db.child("Sources").child("Google Sheets").get().val().items()
        elif str(s_type) == "schedule":
            sources = db.child("Sources").child("AGH Schedules").get().val().items()
        else:
            logging.error("firebase_init.read_source() - unknown argument: %s", s_type)
            return []
        
        new_sources = []
        for k, v in sources:
            new_sources.append(v)
        
        return new_sources
    except:
        logging.error("Getting source of %s type has failed", s_type)
        return []
    
def is_source(source, s_type):
    sources = read_source(s_type)
    for src in sources:
        if str(src)==str(source):
            return 1
    return 0
        
    
def read_sources():
    try:
        htmls = db.child("Sources").child("HTML Links").get().val().items()
    except:
        logging.error("Initial getting html sources from firebase had failed")
        htmls = []
    try:
        excels = db.child("Sources").child("MS Excel Files").get().val().items()
    except:
        logging.error("Initial getting excel sources from firebase had failed")
        excels = []
    try:
        sheets = db.child("Sources").child("Google Sheets").get().val().items()
    except:
        logging.error("Initial getting google sheet sources from firebase had failed")
        sheets = []
    try:
        schedules = db.child("Sources").child("AGH Schedules").get().val().items()
    except:
        logging.error("Initial getting schedules sources from firebase had failed")
        schedules = []
        
    sources = [excels, sheets, htmls, schedules]
    new_sources = []
    for source in sources:
        vals = []
        for k, v in source:
            vals.append(v)
        new_sources.append(vals)
    logging.info("Firebase_init.read_sources(): Sources read successfully")
    return new_sources
    