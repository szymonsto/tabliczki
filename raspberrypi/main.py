import firebase_init
import excel_loading
import sheet_loading
import ke_scrapper
import agh_excel
import url_reading
import concurrent.futures      #threading/multiprocessing
import time
from datetime import datetime, date
import classes
import logging

logging.basicConfig(filename='/home/pi/doorlabels/door_label.log',level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y/%m/%d %H:%M:%S')


#stworzyc plik czytajacy linki/przechowujacy linki/dodajacy linki


def crush_list (input_list):
    output_list = []
    for sublist in input_list:
        if type(sublist)==list:
            for item in sublist:
                output_list.append(item)
        else:
            output_list.append(sublist)
    
    return output_list


def excel_reading(path):
    objects.append(excel_loading.excel_reading(path))
    
def sheet_reading(path):
    objects.append(sheet_loading.google_sheet_reading(path))
    
def html_reading(link):
    objects.append(ke_scrapper.ke_scrapper(link))
    
def schedule_reading(path):
    objects.append(agh_excel.read_schedule(path))

functions = [excel_reading, sheet_reading, html_reading, schedule_reading]
sources = []

def compare_time (t1, t2):      #as follows: old date, new date
    if (t1[0]<t2[0]):
        return 1                #if new is "fresher" then returns 1
    elif (t1[0]>t2[0]):
        return 0
    else:
        if t1[1]<t2[1]:
            return 1
        else:
            return 0
    
def compare_objects(obj):
    if isinstance(obj, classes.Teacher):
        temp = firebase_init.read_teacher(obj.name)
        #temp.show_object()
        if compare_time(temp.update_date, obj.update_date):
            firebase_init.update_teacher(obj)
            return 1
        else:
            return 0
    elif isinstance(obj, classes.Classes):
        temp = firebase_init.read_room(obj.room, obj.weekday, obj.hours[0])
        #temp.show_object()
        if compare_time(temp.update_date, obj.update_date):
            firebase_init.update_room(obj)
            return 1
        else:
            return 0
    else:
        return 0
    

def update_database(obj):
    if isinstance(obj, classes.Teacher):
        if firebase_init.read_teacher(obj.name):
            if compare_objects(obj):
                logging.info("Teacher %s has been successfully updated in firebase", obj.name)
            else:
                logging.info("Teacher %s has nothing to update", obj.name)
        else:
            firebase_init.add_teacher(obj)
            logging.info("Teacher %s has been successfully added to firebase", obj.name)
    elif isinstance(obj, classes.Classes):
        if firebase_init.read_room(obj.room, obj.weekday, obj.hours[0]):
            if compare_objects(obj):
                logging.info("Classes %s, %s %s, has been successfully updated in firebase", obj.room, obj.weekday, obj.hours[0])
        else:
            firebase_init.add_room(obj)
            logging.info("Classes %s, %s %s, has been successfully added to firebase", obj.room, obj.weekday, obj.hours[0])
    else:
        logging.warning("Unknown object cannot be classified and has been omitted")
        return 0
        

def searching (ind):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.map(functions[ind], sources[ind])
        
def check_for_schedule_updates():
    
    day = datetime.now().strftime("%d")
    month = datetime.now().strftime("%m")
    year = datetime.now().strftime("%Y")
    hour = datetime.now().strftime("%H:%M")
    weekday = date(int(year), int(month), int(day)).weekday()
    if weekday==6 and hour=="23:00":
        names = ["_laby_KE_S_Z_", "__sale_S_Z_"]
        link = url_reading.get_website()
        url_objects=[]
        url_reading.starting_search(link)
        url_reading.search_website_for_url(link, url_objects)
        url_reading.ending_search(url_objects, link)
        url_reading.get_file(names, url_objects, link)
    

def main(objects):

    global sources
    sources = firebase_init.read_sources()

    t1 = time.perf_counter()
    # Multithreading used to search for data in various sources
    with concurrent.futures.ThreadPoolExecutor() as executor0:
        future0 = executor0.map(searching, range(len(sources)))
                 
    # Some of returned data contains lists inside list. Crush_list is supposed to get rid of them
    print(objects, "\n")
    objects = crush_list(objects)
    print("\n", objects)
    

    t2 = time.perf_counter()

    print("Time: ", t2-t1)
    
    # Multithreading used to update firebase with multiple ocjects "at the same time"
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.map(update_database, objects)
        
    t3 = time.perf_counter()

    print("Time: ", t3-t2)

if __name__=='__main__':
    
    objects = []
    main(objects)
    check_for_schedule_updates()

