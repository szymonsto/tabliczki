/*
 * logs_manager.h
 */

#ifndef LOGS_MANAGER_H
#define LOGS_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"

using namespace std;

//----------

#define LOGS Logs::getInstance()

//----------

#if APP_USE_LOGS
#define LOG(_module_id, _description, _type) LOGS->reportLog(_module_id, _description, _type)
#else 
#define LOG(_module_id, _description, _type) __NOP();
#endif 

//----------

struct logData
{
  int module;
  char description[APP_LOG_LENGTH];
  enum log_type type;
};

class Logs
{
    public:    
    void reportLog(int module, String description, enum log_type type);
    static Logs* getInstance(void);
    struct logData getData(void);
    void setCommand(Command *command);
    void begin(void);
    void process(void);
    ~Logs(void);
    static Logs *_instance;
    
    private:
    Logs(void);
    struct logData _data;
    Command *_reportType;
};

//----------

#endif /* LOGS_MANAGER_H */