/*
 * serial_port_manager.h
 * 
 * Singleton design pattern
 */

#ifndef SERIAL_PORT_H
#define SERIAL_PORT_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

/* Arduino libraries */
#include "Arduino.h"
#include <cppQueue.h>

using namespace std;

//----------

#define SERIAL_PORT SerialPort::getInstance()

//----------

struct SerialTask
{
    enum serialPortTask task;
    void *data;
};

class SerialPort
{
    public:
    static SerialPort* getInstance(void);
    void begin(void);
    void process(void);
    static SerialPort *_instance;
    struct SerialTask getData(void);

    bool addTask(enum serialPortTask task, void *data);

    private:
    SerialPort(void): taskQueue(sizeof(struct SerialTask), SERIAL_TASK_QUEUE_SIZE, FIFO){ }; 
    Queue taskQueue; 
    struct SerialTask _task;   
};

//----------

#endif /* SERIAL_PORT_H */