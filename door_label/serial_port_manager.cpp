/*
 * serial_port_manager.cpp
 * 
 * Singleton design pattern
 */

#include "serial_port_manager.h"

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "app_manager.h"

/* Arduino libraries */
#include "Arduino.h"
#include <cppQueue.h>
#include "hw.h"

//----------
/* Context */

SerialPort *SerialPort::_instance = NULL;

struct serial_manager_context 
{
    bool readData;
};

static struct serial_manager_context ctx;

//----------

static void handleTask(struct SerialTask task);
static void readData(void);

//----------

void SerialPort::begin(void)
{
    uartInit(APP_SERIAL_PORT_BAUDRATE);
}

void SerialPort::process(void)
{
    struct SerialTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }

    readData();
}

SerialPort* SerialPort::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new SerialPort();
  }

  return _instance;
}

bool SerialPort::addTask(enum serialPortTask task, void *data)
{
    struct SerialTask newTask = DEF_SERIAL_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        return false;
    }    
}

//----------

static void handleTask(struct SerialTask task)
{
    switch(task.task)
    {
        case WRITE:

            uartWrite((char *)task.data);

            MEMORY_MANAGER->deallocate(task.data);
        break;
        case READ:
        break;
        case WAIT_FOR:
        break;
        default:
        break;
    }
}

static void readData(void)
{
    if(uartBufferBytes())
    {
        String data;

        while(uartBufferBytes())
        {
            data += String(uartRead());    
            delay(10);         
        }

        void *buffer;

        buffer = MEMORY_MANAGER->allocate(sizeof(char), data.length()+1);

        data.getBytes((unsigned char *)buffer, data.length()+1);

        APP_MANAGER->addTask(NEW_SERIAL_DATA, buffer);
    }

}

//----------