/*
 * Hardware functions
 */

#include "hw.h"

#include "Arduino.h"
#include "ESP8266WiFi.h"
#include <Wire.h>
#include <MCP7940.h>
#include "utils.h"

//--------
/* Context */

static MCP7940_Class rtc_ctx;

//--------

long long getMillis(void)
{
    return millis();
}

bool uartInit(unsigned int baudrate)
{
    Serial.begin(baudrate);
}

bool uartWrite(char *buffer)
{
    Serial.println(buffer);

    return true;
}

char uartRead(void)
{
    return Serial.read();
}

int uartBufferBytes(void)
{
    return Serial.available();
}

void i2cInit(void)
{
    Wire.begin(14, 12);
}

void rtcGetTimeHw(char data[], int dataLen)
{
    rtc_ctx.now();

    String result = rtc_ctx.hour() + ":" + rtc_ctx.minute();

    setChar(data, result, dataLen);
}

int getBatlvl(void)
{
    return analogRead(analogInPin);
}

void deepSleep(int period)
{
    ESP.deepSleep(period); 
}

//--------