/*
 * Wifi manager
 */
 
#include "wifi_manager.h"

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "app_manager.h"

#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "app_core.h"
#include <cppQueue.h>

//----------
/* Context */

AppWifi *AppWifi::_instance = NULL;

//----------

static void handleTask(struct AppWifiTask task);
static void reportEvent(void);

//----------

AppWifi *AppWifi::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppWifi();
    }

    return _instance;
}

void AppWifi::setAPData(String ssid, String password)
{
    _apData = DEF_APPWIFI_DATA_OBJ(ssid, password);

    _state.isInitialized = true; 
}

void AppWifi::setOwnData(String ssid, String password)
{
    _ownData = DEF_APPWIFI_DATA_OBJ(ssid, password);
}

void AppWifi::setMode(enum wifiManagerMode mode)
{
    switch(mode)
    {
        case CLIENT:
            WiFi.mode(WIFI_STA);
        break;
        case SERVER:
        break;
        case CLIENT_AND_SERVER:
            WiFi.mode(WIFI_STA);
        break;
        default:
        break;
    }
}

void AppWifi::connect(void)
{
    WiFi.begin(_apData.SSID, _apData.password);
}

void AppWifi::disconnect(void)
{
    WiFi.disconnect();
}

int AppWifi::getStatus(void)
{
    return WiFi.status();
}

bool AppWifi::isReadyToConnect(void)
{
    return _state.isInitialized;   
}

void AppWifi::begin(void)
{

}

void AppWifi::process(void)
{
    struct AppWifiTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }

    reportEvent();
}

bool AppWifi::addTask(enum appWifiTasks task, void *data)
{
    struct AppWifiTask newTask = DEF_APPWIFI_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        return false;
    } 
}

//----------

static void handleTask(struct AppWifiTask task)
{
    switch(task.task)
    {
        case CONNECT:
            if(APP_WIFI->isReadyToConnect())
            {
                LOG(1, "Wifi - connecting...", LOG_INFO);
                APP_WIFI->_state.newConnectionRequest = true;
                APP_WIFI->connect();
            }
        break;
        default:
        break;
    }
}

static void reportEvent(void)
{
    if(APP_WIFI->_state.newConnectionRequest)
    {
        if(WiFi.status() == WL_CONNECTED)
        {
            APP_MANAGER->addTask(WIFI_CONNECTED, NULL);

            APP_WIFI->_state.newConnectionRequest = false;
        }        
    }
}

//----------