/*
 * Power manager
 */

/* Wysyła event i po tym odświeżnaja jest odpowienio firebase - wtedy jest wyświetlane */

#ifndef BUTTON_H
#define BUTTON_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

#include "Arduino.h"
#include <cppQueue.h>

//----------

#define BUTTON AppButton::getInstance()

//----------

class AppButton
{
    public:
    static AppButton* getInstance(void);
    void begin(void);
    void process(void);

    private:
    AppButton(void) { }
    static AppButton *_instance;   
};

//----------

#endif /* BUTTON_H */