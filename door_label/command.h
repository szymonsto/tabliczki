/*
 * Commands
 * Command pattern
 */

#ifndef COMMAND_H
#define COMMAND_H

#include <stdlib.h>

//----------

class Command
{
    public:
    Command(void)
    { }
    virtual ~Command(void)
    { }
    virtual void execute(void)=0;
};

//----------

#endif /* COMMAND_H */