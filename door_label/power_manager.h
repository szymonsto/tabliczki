/*
 * Power manager
 */

#ifndef POWER_MANAGER_H
#define POWER_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

#include "Arduino.h"
#include <cppQueue.h>

//----------

#define POWER_MANAGER AppPowerManager::getInstance()

struct AppPowerManagerTask
{
    enum appPowerManagerTask task;
    void *data;
};

//----------

class AppPowerManager
{
    public:
    static AppPowerManager* getInstance(void);
    void begin(void);
    void process(void);
    bool addTask(enum appPowerManagerTask task, void *data);
    int batteryLevel(void);

    private:
    AppPowerManager(void) : taskQueue(sizeof(struct AppPowerManagerTask), APP_POWER_MANAGER_TASK_QUEUE_SIZE, FIFO) { }
    Queue taskQueue; 
    static AppPowerManager *_instance;   
};

//----------

#endif /* POWER_MANAGER_H */