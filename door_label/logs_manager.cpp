/*
 * Logs
 * Singleton pattern
 */

#include "logs_manager.h"

#include "command.h"
#include "logs_events.h"
#include "hw.h"

using namespace std;

//----------
/* Context */

Logs *Logs::_instance = NULL;

//----------
/* Public methods */

Logs::Logs(void)
{
  #if APP_PRINT_LOGS

  Serial.begin(APP_SERIAL_BAUDRATE);
  
  #endif /* APP_PRINT_LOGS */

  #if APP_USE_LOGS_COMMAND == 1

  setCommand(new AppManagerPrintEvent());

  #elif APP_USE_LOGS_COMMAND == 2
  
  setCommand(new AppManagerSaveEvent());

  #elif APP_USE_LOGS_COMMAND == 3

  setCommand(new AppManagerPrintSaveEvent());

  #endif /* APP_USE_LOGS_COMMAND */
}

Logs::~Logs(void)
{
  delete _reportType;
}

void Logs::reportLog(int module, String description, enum log_type type)
{  
  description.getBytes((unsigned char *)_data.description, APP_LOG_LENGTH);
  _data.module = module;
  _data.type = type;  
  #if APP_PRINT_LOGS
  
  Serial.print("Module: ");
  Serial.print(module);
  switch(type)
  {
    case LOG_INFO:
      Serial.print(" Info: ");
      cout<<"INFO"<<endl;
    break;
    case LOG_WARNING:
      Serial.print(" Warning: ");
    break;
    case LOG_ERROR:
      Serial.print(" Error: ");
    break;
    default:
    break;
  }
  
  Serial.println(description);
  
  #endif /* APP_PRINT_LOGS */ 

  if(this->_reportType)
  {
    this->_reportType->execute();
  }
}

void Logs::process(void)
{ }

void Logs::begin(void)
{ }

Logs* Logs::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new Logs();
  }

  return _instance;
}

struct logData Logs::getData(void)
{
  return _data;
}

void Logs::setCommand(Command *command)
{
  _reportType = command;
}

//----------