/*
 * logs_events.h
 */

#ifndef LOGS_EVENTS_H
#define LOGS_EVENTS_H

#include <stdlib.h>

#include "app_core.h"
#include "app_config.h"
#include "command.h"

//----------

class AppManagerPrintEvent : public Command
{
    public:
    AppManagerPrintEvent(void);  
    ~AppManagerPrintEvent(void);  
    void execute(void);
};

class AppManagerSaveEvent : public Command
{
    public:
    AppManagerSaveEvent(void); 
    ~AppManagerSaveEvent(void);
    void execute(void); 
};

class AppManagerPrintSaveEvent : public Command
{
    public:
    AppManagerPrintSaveEvent(void);
    ~AppManagerPrintSaveEvent(void);  
    void execute(void); 
};

//----------

#endif /* LOGS_EVENTS_H */