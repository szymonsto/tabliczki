/*
 * Device
 */

#include "device.h"

#include "app_manager.h"
#include "firebase_manager.h"

//----------

AppDevice *AppDevice::_instance = NULL;

//----------

AppDevice *AppDevice::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppDevice();
    }

    return _instance;
}

void AppDevice::begin(void)
{ }

void AppDevice::process(void)
{
    if( (status.wifiConnected == false) &&
        (status.wifiReported == false) &&
        (String(data.apSsid) != "!") &&
        (String(data.apPasswd) != "!"))
    {
        if(status.isRepeater)
        {
            APP_MANAGER->addTask(WIFI_CONNECTION_REQUEST_CLIENT_SERVER, NULL);
        }
        else
        {
            APP_MANAGER->addTask(WIFI_CONNECTION_REQUEST_CLIENT, NULL);

            LOG(12, "DEVICE: Wifi connect", LOG_INFO);
        }     

        status.wifiReported = true;
    }

    if( (status.wifiConnected) &&
        (status.baseConnected == false) &&
        (status.baseReported == false) &&
        (String(data.baseDomain) != "!") &&
        (String(data.baseAuth) != "!"))
    {
        APP_FIREBASE->setHostData(String(data.baseDomain), String(data.baseAuth));
        APP_MANAGER->addTask(FIREBASE_CONNECTION_REQUST, NULL);

        LOG(12, "DEVICE: Firebase connect", LOG_INFO);

        status.baseReported = true;
    }

    struct AppDeviceTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

bool AppDevice::addTask(enum appDeviceTask task, void *data)
{
    struct AppDeviceTask newTask = DEF_DEVICE_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        LOG(8, "Add Task failed", LOG_WARNING);
        return false;
    } 
}

void AppDevice::handleTask(struct AppDeviceTask task)
{
    switch(task.task)
    {
        case WIFI_CONNECTED_DEVICE:
            status.wifiConnected = true;    
            status.wifiReported = false;
            LOG(12, "DEVICE: Wifi connected", LOG_INFO);   
        break;
        case BASE_CONNECTED_DEVICE:
            status.baseConnected = true;
            status.baseReported = false;
            LOG(12, "DEVICE: Firebase connected", LOG_INFO);   
        break;
        default:
            LOG(12, "Unsupported Task", LOG_INFO); 
        break;
    }
}

//----------



//----------