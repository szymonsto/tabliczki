/*
 * Hardware functions
 */

#ifndef HW_H
#define HW_H

#include "app_core.h"

#include "ESP8266WiFi.h"
#include "Wire.h"

//-------- 

#define RST     0xFF
#define ADC0    0xFF
#define ENABLE  0xFF
#define GPIO16  16
#define GPIO14  14
#define GPIO12  12
#define GPIO13  13
#define GPIO11  11
#define GPIO7   7
#define GPIO9   9
#define GPIO10  10
#define GPIO8   8
#define GPIO6   6
#define GPIO15  15
#define GPIO2   2
#define GPIO0   0
#define GPIO4   4
#define GPIO5   5
#define GPIO3   3
#define GPIO1   1

//--------

#define PIN_BAT_LVL     ADC0
#define PIN_REST        GPIO16
#define PIN_SCL         GPIO14
#define PIN_SDA         GPIO12
#define PIN_RST         GPIO13
#define PIN_PWR         GPIO11
#define PIN_CS          GPIO7
#define PIN_DC          GPIO9
#define PIN_BUSY        GPIO10
#define PIN_SDI         GPIO8
#define PIN_SCLK        GPIO6
#define PIN_WAKEUP_EN   GPIO15
#define PIN_BUT3        GPIO2
#define PIN_BUT1        GPIO0
#define PIN_BUT2        GPIO4
#define PIN_PWR_HOLD    GPIO5
#define PIN_RX          GPIO3
#define PIN_TX          GPIO1

//--------

long long getMillis(void);

bool uartInit(unsigned int baudrate);

bool uartWrite(char *buffer);

char uartRead(void);

int uartBufferBytes(void);

void i2cInit(void);

void i2cRead(void);

void i2cWrite(void);

void rtcGetTimeHw(char data[], int dataLen);

int getBatlvl(void);

void deepSleep(int period);

//--------

#endif /* HW_H */