/*
 * utils.h
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

#include "app_core.h"
#include "app_config.h"
#include "logs_manager.h"

#include "Arduino.h"

//----------

#ifndef INT_MAX
#define INT_MAX 0xffffffff
#endif /* INT_MAX */

//----------

bool timeElapsed(long long timestamp, long long period);

bool findSubstring(String string, String subString);

int findStringChar(String string, char character);

void setChar(char data[], String str, int dataLen);

int compareDate(String time1, String weekDay1, String time2, String weekDay2);

int getTabIndex(String tab[], int tabSize, String value);

//----------

#endif /* UTILS_H */
