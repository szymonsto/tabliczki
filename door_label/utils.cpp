/*
 * utils.cpp
 */

#include "utils.h"

#include "hw.h"
#include "app_core.h"

//----------

static String hourTab[] = 
{
  [HOUR_8_00] = "8:00",
  [HOUR_9_35] = "9:35",
  [HOUR_11_15] = "11:15",
  [HOUR_12_50] = "12:50",
  [HOUR_14_40] = "14:40",
  [HOUR_16_15] = "16:15",
  [HOUR_17_50] = "17:50",
  [HOUR_19_25] = "19:25",  
};

static String weekdayTab[] = 
{
  [MONDAY] = "Monday",
  [TUESDAY] = "Tuesday",
  [WEDNESDAY] = "Wednesday",
  [THURSDAY] = "Thursday",
  [FRIDAY] = "Friday",
};

//----------
/* Time */

bool timeElapsed(long long timestamp, long long period)
{
    if((getMillis() - timestamp) > period)
    {
        return true;
    }
    else
    {
        return false;
    }    
}

//----------

bool findSubstring(String string, String subString)
{
    for(int i = 0; i < string.length(); i++)
    {
        if(string[i] == subString[0])
        {
            for(int j = 0; j < subString.length(); j++)
            {
                if(string[j + i] != subString[j])
                {
                    break;
                }

                if(j == subString.length() - 1)
                {
                    return true;
                }
            }
        }
    }

    return false;
}

int findStringChar(String string, char character)
{
    for(int i = 0; i < string.length(); i++)
    {
        if(string[i] == character)
        {
            return i;
        }
    }

    return 0;
}

void setChar(char data[], String str, int dataLen)
{
    str.getBytes((unsigned char *)data, dataLen);
}

int compareDate(String time1, String weekDay1, String time2, String weekDay2)
{
    int weekDay1Index = getTabIndex(weekdayTab, ARRAY_SIZE(weekdayTab), weekDay1);
    int weekDay2Index = getTabIndex(weekdayTab, ARRAY_SIZE(weekdayTab), weekDay2);
    int time1Index = getTabIndex(hourTab, ARRAY_SIZE(hourTab), time1);
    int time2Index = getTabIndex(hourTab, ARRAY_SIZE(hourTab), time2);

    int date1 = (weekDay1Index * ARRAY_SIZE(weekdayTab)) + time1Index;
    int date2 = (weekDay2Index * ARRAY_SIZE(weekdayTab)) + time2Index;

    return date2 - date1;
}

int getTabIndex(String tab[], int tabSize, String value)
{
    if(tab == NULL)
    {
        return INT_MAX; // error
    }

    for(int i = 0; i < tabSize; i++)
    {
        if(tab[i] == value)
        {
            return i;
        }
    }

    return INT_MAX; 
}

//----------
