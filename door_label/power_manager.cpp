/*
 * Device
 */

#include "power_manager.h"

#include "app_manager.h"
#include "hw.h"

//----------

AppPowerManager *AppPowerManager::_instance = NULL;

//----------

static void handleTask(struct AppPowerManagerTask task);

//----------

AppPowerManager *AppPowerManager::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppPowerManager();
    }

    return _instance;
}

void AppPowerManager::begin(void)
{ 
    WritePin(PIN_PWR_HOLD, HIGH);
}

void AppPowerManager::process(void)
{
    struct AppPowerManagerTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

bool AppPowerManager::addTask(enum appPowerManagerTask task, void *data)
{
    struct AppPowerManagerTask newTask = DEF_POWER_MANAGER_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        LOG(8, "Add Task failed", LOG_WARNING);
        return false;
    } 
}

int AppPowerManager::batteryLevel(void)
{
    DEVICE->data.batLvl = getBatLvl();

    return 0;
}

//----------

static void handleTask(struct AppPowerManagerTask task)
{
    switch(task.task)
    {
        case DEEP_SLEEP_MODE:
            deepDleep(DEVICE->data.updatePeriod);
        break;
        default:
            // log: unsupported
        break;
    }
}

//----------