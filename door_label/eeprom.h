/*
 * EEPROM
 */

#ifndef EEPROM_H
#define EEPROM_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "state.h"

/* Arduino libraries */
#include <ESP8266WiFi.h>
#include "Arduino.h"
#include "cppQueue.h"

using namespace std;

//----------

#define APP_EEPROM AppEeprom::getInstance()

//----------

struct AppEepromTask
{
    enum appEepromTask task;
    void *data;
};

class AppEeprom
{
    public:
    static AppEeprom* getInstance(void);
    void begin(void);
    void process(void);
    static AppEeprom *_instance;
    int memory_pointer;

    bool addTask(enum appEepromTask task, void *data);

    private:
    AppEeprom(void): taskQueue(sizeof(struct AppRtcTask), APP_EEPROM_TASK_QUEUE_SIZE, FIFO) { }
    Queue taskQueue;        
};

//----------

#endif /* EEPROM_H */