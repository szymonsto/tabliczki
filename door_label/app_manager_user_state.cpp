/*
 * App manager
 */

#include "app_manager_user_state.h"

#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "serial_port_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "firebase_manager.h"
#include "app_manager.h"
#include "utils.h"
#include "app_manager_config_state.h"
#include "display_manager.h"
#include "rtc.h"
#include "device.h"
#include "button.h"
#include "eeprom.h"
#include "power_manager.h"

/* Arduino libraries */
#include "Arduino.h"
#include "cppQueue.h"

//----------
/* Context */

AppManagerStateUser *AppManagerStateUser::_instance = NULL;

//----------

#define DEBUG_MODE_START_SEQUENCE "start"

//----------

static void printLogRequest(void *data);
static String logToString(struct logData newLog);
static void printDisplayContentData(void);
static int findClosestHourInBase(String time, String weekDay);

static char *sLogTypes[] = 
{
  [LOG_INFO] = "INFO: ",
  [LOG_WARNING] = "WARNING: ",
  [LOG_ERROR] = "ERROR: ",
};

static String hourTab[] = 
{
  [HOUR_8_00] = "8:00",
  [HOUR_9_35] = "9:35",
  [HOUR_11_15] = "11:15",
  [HOUR_12_50] = "12:50",
  [HOUR_14_40] = "14:40",
  [HOUR_16_15] = "16:15",
  [HOUR_17_50] = "17:50",
  [HOUR_19_25] = "19:25",  
};

static String weekdayTab[] = 
{
  [MONDAY] = "Monday",
  [TUESDAY] = "Tuesday",
  [WEDNESDAY] = "Wednesday",
  [THURSDAY] = "Thursday",
  [FRIDAY] = "Friday",
};

//----------

void AppManagerStateUser::begin(void)
{
  SERIAL_PORT->begin();
  LOGS->begin();
  MEMORY_MANAGER->begin();
  APP_WIFI->begin();  
  APP_FIREBASE->begin();
  APP_DISPLAY->begin();
  APP_RTC->begin();
  DEVICE->begin();
  BUTTON->begin();
  POWER_MANAGER->begin();

  APP_DISPLAY->addTask(CLEAR, NULL);
  APP_RTC->addTask(GET_TIME, NULL);

  LOG(1, "Successful initialization", LOG_INFO);
}

void AppManagerStateUser::process(void)
{
  DEVICE->process();
  SERIAL_PORT->process();
  LOGS->process();
  MEMORY_MANAGER->process();  
  APP_WIFI->process();
  APP_FIREBASE->process();
  APP_DISPLAY->process();
  APP_RTC->process();
  BUTTON->process();
  APP_EEPROM->process();

  handleTasks();
}

bool AppManagerStateUser::addTask(enum appManagerTask task, void *data)
{
    struct AppManagerTask newTask = DEF_APP_MANAGER_DATA_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        return false;
    }  
}

void AppManagerStateUser::handleSerialData(void *data)
{
  String string = String((char *)data);
  String subString = String(DEBUG_MODE_START_SEQUENCE);

  if(findSubstring(string, subString))
  {
    LOG(15, "Config mode", LOG_INFO);

    String sAck = "ack";
    void *newAck;
    newAck = MEMORY_MANAGER->allocate(sizeof(char), sAck.length() + 1);

    sAck.getBytes((unsigned char *)newAck, sAck.length() + 1);
    SERIAL_PORT->addTask(WRITE, newAck);

    APP_MANAGER->transitionToState(APP_MANAGER_STATE_CONFIG);
  }
}

AppManagerStateUser* AppManagerStateUser::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new AppManagerStateUser();
  }

  return _instance;
}

void AppManagerStateUser::transitionSetup(void)
{
    LOG(10, "User - transition setup", LOG_INFO);
  
    APP_DISPLAY->addTask(CLEAR, NULL);
    APP_FIREBASE->addTask(REFERSH_ROOM_DATA, NULL);
}

void AppManagerStateUser::printDisplayContentData(void)
{
  struct DisplayContentData *newData;
  newData = (struct DisplayContentData *)MEMORY_MANAGER->allocate(sizeof(struct DisplayContentData), 3); /* Display 3 classes */

  struct DisplayContentData sData;
  String temp;

  int roomsdataIndex = 0;

  for(int i = 0; i < 2; i++)
  {
    if(i < _noFirebaseElements)
    {
      roomsdataIndex = (_firebaseElementIndex + i) % APP_FIREBASE->_roomsData.size();

      temp = String(APP_FIREBASE->_roomsData[roomsdataIndex].hoursBeg) + "-"+ String(APP_FIREBASE->_roomsData[roomsdataIndex].hoursEnd) + " " + String(APP_FIREBASE->_roomsData[roomsdataIndex].weekday);
      temp.getBytes((unsigned char *)sData.time, APP_DISPLAY_DATA_LENGTH);

      temp = String(APP_FIREBASE->_roomsData[roomsdataIndex].c_names);
      temp.getBytes((unsigned char *)sData.data, APP_DISPLAY_DATA_LENGTH);

      temp = String(APP_FIREBASE->_roomsData[roomsdataIndex].t_names);
      temp.getBytes((unsigned char *)sData.person, APP_DISPLAY_DATA_LENGTH);
    }
    else
    {
      temp = "";
      temp.getBytes((unsigned char *)sData.time, APP_DISPLAY_DATA_LENGTH);
      temp.getBytes((unsigned char *)sData.data, APP_DISPLAY_DATA_LENGTH);
      temp.getBytes((unsigned char *)sData.person, APP_DISPLAY_DATA_LENGTH);
    }  

    memcpy(newData + i, &sData, sizeof(struct DisplayContentData));
  }

  LOG(8, "Send content data to Display", LOG_INFO);
  LOG(8, "Display data: " + String(sData.time), LOG_INFO);
  LOG(8, "Display course: " + String(sData.data), LOG_INFO);
  LOG(8, "Display person: " + String(sData.person), LOG_INFO);

  APP_DISPLAY->addTask(DRAW_CONTENT, (void *)newData);
  APP_DISPLAY->addTask(DRAW_BAR, NULL);
  APP_DISPLAY->addTask(DISPLAY_DATA, NULL);
}

void AppManagerStateUser::handleTasks(void)
{
    struct SerialTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

void AppManagerStateUser::handleTask(struct SerialTask task)
{
  switch(task.task)
  {
    case PRINT_LOG:
      printLogRequest(task.data);
    break;
    case SAVE_LOG:
    break;
    case WIFI_CONNECTED:
      LOG(10, "Wifi - connected", LOG_INFO);   
      MEMORY_MANAGER->deallocate(task.data);
      DEVICE->addTask(WIFI_CONNECTED_DEVICE, NULL);
    break;
    case NEW_SERIAL_DATA:
      handleSerialData(task.data);      
      MEMORY_MANAGER->deallocate(task.data);
    break;
    case FIREBASE_SCAN_END:
      LOG(10, "Firebase - number of elements: " + String(*(int *)task.data), LOG_INFO);
      _firebaseElementIndex = findClosestHourInBase(DEVICE->data.currentTime, DEVICE->data.currentDay);
      _noFirebaseElements = *(int *)task.data;
      APP_FIREBASE->addTask(FIREBASE_GET_ROOM_DATA, NULL);
      APP_RTC->addTask(GET_TIME, NULL);
      MEMORY_MANAGER->deallocate(task.data);
      DEVICE->addTask(BASE_CONNECTED_DEVICE, NULL);
    break;
    case FIREBASE_DATA:
      LOG(10, "New Firebase data", LOG_INFO);
      printDisplayContentData();
      MEMORY_MANAGER->deallocate(task.data);
    break;
    case RTC_DATA:
      LOG(10, "New RTC data", LOG_INFO);
    break;
    case WIFI_CONNECTION_REQUEST_CLIENT:
      APP_WIFI->setAPData(DEVICE->data.apSsid, DEVICE->data.apPasswd);
      APP_WIFI->setMode(CLIENT);
      APP_WIFI->addTask(CONNECT, NULL);
    break;
    case WIFI_CONNECTION_REQUEST_CLIENT_SERVER:
    break;
    case FIREBASE_CONNECTION_REQUST:
      APP_FIREBASE->setHostData(DEVICE->data.baseDomain, DEVICE->data.baseAuth);
      APP_FIREBASE->addTask(BEGIN, NULL);
      
    break;
    case BUTTON_LEFT_PRESS:

    _firebaseElementIndex--;

    if(_firebaseElementIndex < 0)
    {
      _firebaseElementIndex = APP_FIREBASE->_roomsData.size() - 1;
    }

    printDisplayContentData();

    break;
    case BUTTON_MID_PRESS:
      APP_FIREBASE->addTask(REFERSH_ROOM_DATA, NULL);
    break;
    case BUTTON_RIGHT_PRESS:
    _firebaseElementIndex = (_firebaseElementIndex - 1) % _noFirebaseElements;

    printDisplayContentData();

    break;
    default:
      LOG(10, "Unsupported task", LOG_WARNING);
    break;
  }
}

//----------

static void printLogRequest(void *data)
{
  String sLog = logToString(*(struct logData *)data);

  MEMORY_MANAGER->deallocate(data);

  void *newLog;
  newLog = MEMORY_MANAGER->allocate(sizeof(char), sLog.length() + 1);

  sLog.getBytes((unsigned char *)newLog, sLog.length() + 1);

  SERIAL_PORT->addTask(WRITE, newLog);
}

static String logToString(struct logData newLog)
{
  String sLog = sLogTypes[newLog.type];

  sLog.concat(newLog.description);

  return sLog;
}

static int findClosestHourInBase(String time, String weekDay)
{
  int minValue = INT_MAX, minIndex = INT_MAX;
  int dateDifference = 0;

  for(int i  = 0; i < APP_FIREBASE->_roomsData.size(); i++)
  {
    dateDifference = abs(compareDate(time, weekDay, String(APP_FIREBASE->_roomsData[i].hoursBegFromPath), String(APP_FIREBASE->_roomsData[i].weekdayFromPath)));

    if(dateDifference < minValue)
    {
      minValue = dateDifference;
      minIndex = i;
    }
  }
  return minIndex;
}

//----------