/*
 * Device
 */

#ifndef DEVICE_H
#define DEVICE_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "utils.h"

#include "Arduino.h"
#include <cppQueue.h>

//----------

#define DEVICE AppDevice::getInstance()

struct UserData
{
    char room[APP_USER_DATA_LENGTH];
    char apSsid[APP_USER_DATA_LENGTH];
    char apPasswd[APP_USER_DATA_LENGTH];
    char devSsid[APP_USER_DATA_LENGTH];
    char devPasswd[APP_USER_DATA_LENGTH];
    char baseDomain[APP_USER_DATA_LENGTH];
    char baseAuth[APP_USER_DATA_LENGTH];
    char updateTime[APP_USER_DATA_LENGTH];
    char batLvl[APP_USER_DATA_LENGTH];
    char devName[APP_USER_DATA_LENGTH];
    int updatePeriod;
    char currentTime[APP_USER_DATA_LENGTH];
    char currentDay[APP_USER_DATA_LENGTH];
    int batLvl;
};

struct UserStatus
{
    bool wifiConnected;
    bool baseConnected;
    bool wifiReported;
    bool baseReported;
    bool isRepeater;
};

struct AppDeviceTask
{
    enum appDeviceTask task;
    void *data;
};

//----------

class AppDevice
{
    public:
    static AppDevice* getInstance(void);
    void begin(void);
    void process(void);
    bool addTask(enum appDeviceTask task, void *data);

    struct UserData data;
    struct UserStatus status;

    private:
    Queue taskQueue; 
    static AppDevice *_instance; 
    
    AppDevice(void) : taskQueue(sizeof(struct AppDeviceTask), APP_DEVICE_TASK_QUEUE_SIZE, FIFO)
    {
        setChar(data.room, "!", APP_USER_DATA_LENGTH);
        setChar(data.apSsid, "!", APP_USER_DATA_LENGTH);
        setChar(data.apPasswd, "!", APP_USER_DATA_LENGTH);
        setChar(data.devSsid, "!", APP_USER_DATA_LENGTH);
        setChar(data.devPasswd, "!", APP_USER_DATA_LENGTH);
        setChar(data.baseDomain, "!", APP_USER_DATA_LENGTH);
        setChar(data.baseAuth, "!", APP_USER_DATA_LENGTH);
        setChar(data.updateTime, "!", APP_USER_DATA_LENGTH);
        setChar(data.batLvl, "!", APP_USER_DATA_LENGTH);
        setChar(data.devName, "!", APP_USER_DATA_LENGTH);
        data.updatePeriod = 0;
        status.wifiConnected = false;
        status.baseConnected = false;
        status.isRepeater = false;
        status.wifiReported = false;
        status.baseReported = false;
    }

    void handleTask(struct AppDeviceTask task);
};

//----------

#endif /* DEVICE_H */