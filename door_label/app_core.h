/*
 * app core
 * Contains public structures, macros, settings ...
 */

#ifndef APP_CORE_H
#define APP_CORE_H

#include "Arduino.h"

#include "app_config.h"

//----------
/* Modules */

#define MODULE_LOGS 0x6
#define MODULE_UTILS 0x80

//----------
/* Global */

#define ARRAY_SIZE(_tab) (sizeof(_tab)/sizeof(_tab[0]))

#define __NOP() __asm__("nop\n\t")

struct DeviceInfo
{
  char connectedTo[APP_DEVICE_INFO_LENGTH];
  char passowrd[APP_DEVICE_INFO_LENGTH];
  char ssid[APP_DEVICE_INFO_LENGTH];
  int battery;
  int refreshFreq;
  bool isRepeater;
  char room[APP_DEVICE_INFO_LENGTH];
  char weekday[APP_DEVICE_INFO_LENGTH];
};

struct date
{
  int year;
  int month;
  int day;
};

struct time
{
  int hour;
  int min;
  int sec;
};

#define DEF_APP_MANAGER_DATA_OBJ(_task, _data)        \
  {                                                   \
    .task = _task,                                    \
    .data = _data,                                    \
  }                                                   \

//----------
/* Logs module */

enum log_type
{
  LOG_INFO,
  LOG_WARNING,
  LOG_ERROR,  
};

//----------
/* Wifi module */

enum wifiManagerMode
{
  CLIENT,
  SERVER,
  CLIENT_AND_SERVER,
};

#define DEF_APPWIFI_DATA_OBJ(_ssid, _password)      \
  {                                                 \
    .SSID = _ssid,                                  \
    .password = _password,                          \
  }                                                 \


#define DEF_APPWIFI_TASK_OBJ(_task, _data)          \
  {                                                 \
    .task = _task,                                  \
    .data = _data,                                  \
  }                                                 \


//----------
/* Serial port */

#define DEF_SERIAL_TASK_OBJ(_task, _data)           \
  {                                                 \
    .task = _task,                                  \
    .data = _data,                                  \
  }                                                 \


//----------
/* Firebase */

#define FIREBASE_VECTOR_STORAGE_SIZE 20

#define DEF_FIREBASE_DATA_OBJ(_firebaseHost, _firebaseAuth)         \
  {                                                                 \
    .firebaseHost = _firebaseHost,                                  \
    .firebaseAuth = _firebaseAuth,                                  \
  }                                                                 \


#define DEF_FIREBASE_TASK_OBJ(_task, _data)                         \
  {                                                                 \ 
    .task = _task,                                                  \
    .data = _data,                                                  \
  }                                                                 \


//#define FIREBASE_ROOM_NODE_PATH(_roomId, _weekday, _hour, _property)  "/Room/" #_roomId "/" _weekday "/" _hour "/" _property

struct RoomData
{
  char c_names[APP_ROOM_DATA_LENGTH];
  char hoursBeg[APP_ROOM_DATA_LENGTH];
  char hoursEnd[APP_ROOM_DATA_LENGTH];
  char mails[APP_ROOM_DATA_LENGTH];
  char room[APP_ROOM_DATA_LENGTH];
  char t_names[APP_ROOM_DATA_LENGTH];
  char UpdateDate[APP_ROOM_DATA_LENGTH];
  char UpdateHour[APP_ROOM_DATA_LENGTH];
  char weekday[APP_ROOM_DATA_LENGTH];

  char weekdayFromPath[APP_ROOM_DATA_LENGTH];
  char hoursBegFromPath[APP_ROOM_DATA_LENGTH];
};

enum weekdayT // TODO: change name
{
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
};

enum hourT  // TODO: change name
{
  HOUR_8_00,
  HOUR_9_35,
  HOUR_11_15,
  HOUR_12_50,
  HOUR_14_40,
  HOUR_16_15,
  HOUR_17_50,
  HOUR_19_25,
};

enum propertyT // TODO: change name
{
  C_NAMES,
  HOUR_BEG,
  HOUR_END,
  MAILS,
  ROOM,
  T_NAMES,
  UPDATE_DATE,
  UPDATE_HOUR,
  WEEKDAY,
};

//----------
/* Memory manager */

#define MEMORY_MANAGER_VECTOR_STORAGE_SIZE 20

//----------
/* Display */

struct DisplayContentData
{
  char time[APP_DISPLAY_DATA_LENGTH];
  char data[APP_DISPLAY_DATA_LENGTH];
  char person[APP_DISPLAY_DATA_LENGTH];
};

struct DisplayBarData
{
  char updateTime[APP_DISPLAY_DATA_LENGTH];
  char wifiStatus[APP_DISPLAY_DATA_LENGTH];
};

//----------
/* RTC */

#define DEF_APP_RTC_TASK_OBJ(_task, _data)                          \
  {                                                                 \ 
    .task = _task,                                                  \
    .data = _data,                                                  \
  }                                                                 \


//----------
/* Device */

#define DEF_DEVICE_TASK_OBJ(_task, _data)                           \
  {                                                                 \ 
    .task = _task,                                                  \
    .data = _data,                                                  \
  }                                                                 \


//----------
/* Power manager */

#define DEF_POWER_MANAGER_TASK_OBJ(_task, _data)                    \
  {                                                                 \ 
    .task = _task,                                                  \
    .data = _data,                                                  \
  }                                                                 \

//----------
/* EEPROM */

#define DEF_EEPROM_TASK_OBJ(_task, _data)                           \
  {                                                                 \ 
    .task = _task,                                                  \
    .data = _data,                                                  \
  }                                                                 \

//----------

#endif /* APP_CORE_H */

 