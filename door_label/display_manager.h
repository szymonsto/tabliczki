/*
 * Display manager
 */

#ifndef DISPLAY_MANAGER_H
#define DISPLAY_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

#include "Arduino.h"
#include <cppQueue.h>
#include "DEV_Config.h"
#include "EPD.h"
#include "GUI_Paint.h"

//----------

#define APP_DISPLAY AppDisplay::getInstance()

//----------

struct AppDisplayTask
{
    enum appDisplayTask task;
    void *data;
};

//----------

class AppDisplay
{
    public:
    static AppDisplay* getInstance(void);
    bool addTask(enum appDisplayTask task, void *data);
    void begin(void);
    void process(void);    
    UBYTE *blackImage;
    UBYTE *redImage; 

    private:
    AppDisplay(void): taskQueue(sizeof(struct AppDisplayTask), DISPLAY_TASK_QUEUE_SIZE, FIFO){ };
    Queue taskQueue; 
    static AppDisplay *_instance;
   
};

//----------

#endif /* DISPLAY_MANAGER_H */