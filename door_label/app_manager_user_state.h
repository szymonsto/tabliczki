/*
 * App manager state
 */

#ifndef APP_MANAGER_USER_STATE_H
#define APP_MANAGER_USER_STATE_H

#include "state.h"

#include "app_manager.h"
#include <cppQueue.h>

//----------

#define APP_MANAGER_STATE_USER AppManagerStateUser::getInstance()

//----------

class AppManagerStateUser: public State
{
    public:
    void begin(void);
    void process(void);
    bool addTask(enum appManagerTask task, void *data);
    static AppManagerStateUser* getInstance(void);
    static AppManagerStateUser *_instance;

    void transitionSetup(void);

    int _noFirebaseElements; /* TODO move to private section */
    int _firebaseElementIndex; /* TODO move to private section */

    private:
    void handleSerialData(void *data);
    AppManagerStateUser(): taskQueue(sizeof(struct AppManagerTask), APP_MANAGER_TASK_QUEUE_SIZE, FIFO) { }
    Queue taskQueue; 
    ~AppManagerStateUser() {}   
    
    void initUserData(void);
    void initDevice(void);  
    void printDisplayContentData(void);
    void handleTasks(void);
    void handleTask(struct SerialTask task);
};

//----------

#endif /* APP_MANAGER_USER_STATE_H */

