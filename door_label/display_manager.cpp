/*
 * Display manager
 */

#include "display_manager.h"

#include "DEV_Config.h"
#include "EPD.h"
#include "GUI_Paint.h"
#include "memory_manager.h"
#include "power_manager.h"
#include "Arduino.h"
#include <SPI.h>
#include "rtc.h"
#include "device.h"
#include "utils.h"

//----------

#define DISPLAY_BAR_HEIGHT 30
#define DISPLAY_CONTENT_HEIGHT 65
#define DISPLAY_BAR_PX_X(_px)  (_px)
#define DISPLAY_BAR_PX_Y(_px)  (_px)
#define DISPLAY_CONTENT_1_PX_X(_px)  (_px)
#define DISPLAY_CONTENT_1_PX_Y(_px)  (_px + DISPLAY_BAR_HEIGHT)
#define DISPLAY_CONTENT_2_PX_X(_px)  (_px)
#define DISPLAY_CONTENT_2_PX_Y(_px)  (_px + DISPLAY_BAR_HEIGHT + DISPLAY_CONTENT_HEIGHT)
#define DISPLAY_CONTENT_3_PX_X(_px)  (_px)
#define DISPLAY_CONTENT_3_PX_Y(_px)  (_px + DISPLAY_BAR_HEIGHT + (2 * DISPLAY_CONTENT_HEIGHT))

//----------

AppDisplay *AppDisplay::_instance = NULL;

static UBYTE *BlackImage, *RedImage;

//----------

static void handleTask(struct AppDisplayTask task);
static void clearDisplay(void);
static void printBar(struct DisplayBarData *data);
static void printContent(struct DisplayContentData *data);
static void displayData(void);

//----------

AppDisplay *AppDisplay::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppDisplay();
    }

    return _instance;
}

void AppDisplay::begin(void)
{       
    SPI.begin();
    pinMode(EPD_BUSY_PIN,  INPUT);
    pinMode(EPD_RST_PIN, OUTPUT);
    pinMode(EPD_DC_PIN, OUTPUT);
    
    pinMode(EPD_CS_PIN, OUTPUT);

    digitalWrite(EPD_CS_PIN, HIGH);

    EPD_2IN7B_Init();  
    EPD_2IN7B_Clear();

    UWORD Imagesize = ((EPD_2IN7B_WIDTH % 8 == 0)? (EPD_2IN7B_WIDTH / 8 ): (EPD_2IN7B_WIDTH / 8 + 1)) * EPD_2IN7B_HEIGHT;
    if((BlackImage = (UBYTE *)malloc(Imagesize)) == NULL) {
        printf("Failed to apply for black memory...\r\n");
        while(1);
    }
    if((RedImage = (UBYTE *)malloc(Imagesize)) == NULL) {
        printf("Failed to apply for red memory...\r\n");
        while(1);
    }
    printf("NewImage:blackImage and redImage\r\n");
    Paint_NewImage(BlackImage, EPD_2IN7B_WIDTH, EPD_2IN7B_HEIGHT, 270, WHITE);
    Paint_NewImage(RedImage, EPD_2IN7B_WIDTH, EPD_2IN7B_HEIGHT, 270, WHITE);

    //Select Image
    Paint_SelectImage(BlackImage);
    Paint_Clear(WHITE);
    Paint_SelectImage(RedImage);
    Paint_Clear(WHITE);
}

void AppDisplay::process(void)
{
    struct AppDisplayTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

bool AppDisplay::addTask(enum appDisplayTask task, void *data)
{
    struct AppDisplayTask newTask = DEF_FIREBASE_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        LOG(8, "Add Task failed", LOG_WARNING);
        return false;
    } 
}

//----------

static void handleTask(struct AppDisplayTask task)
{
    switch(task.task)
    {
        case CLEAR:
            clearDisplay();     
        break;
        case DRAW_BAR:
            printBar((struct DisplayBarData *)task.data);
            MEMORY_MANAGER->deallocate(task.data);
        break;
        case DRAW_CONTENT:
            printContent((struct DisplayContentData *)task.data);
            MEMORY_MANAGER->deallocate(task.data);
        break;
        case DISPLAY_DATA:
            displayData();
        break;
        default:
            LOG(20, "Unsupported task", LOG_WARNING);
        break;
    }
}

static void clearDisplay(void)
{
    Paint_SelectImage(BlackImage);
    Paint_Clear(WHITE);
    Paint_SelectImage(RedImage);
    Paint_Clear(WHITE);

    EPD_2IN7B_Clear();

    LOG(20, "Clear display", LOG_INFO);
}

static void printBar(struct DisplayBarData *data)
{
    Paint_SelectImage(BlackImage);

    char time[30];
    setChar(time, String("Last update: " + String(DEVICE->data.currentTime)), 30);

    Paint_DrawString_EN(DISPLAY_BAR_PX_X(0), DISPLAY_BAR_PX_Y(0), time, &Font16, WHITE, BLACK);

    Paint_DrawLine(DISPLAY_CONTENT_1_PX_X(0), DISPLAY_CONTENT_1_PX_Y(0), DISPLAY_CONTENT_1_PX_X(EPD_2IN7B_HEIGHT - 10), DISPLAY_CONTENT_1_PX_Y(0), BLACK, DOT_PIXEL_3X3, LINE_STYLE_SOLID);
}

static void printContent(struct DisplayContentData *data)
{
    Paint_SelectImage(BlackImage);
    Paint_DrawString_EN(DISPLAY_CONTENT_1_PX_X(0), DISPLAY_CONTENT_1_PX_Y(5), data[0].time, &Font16, WHITE, BLACK);
    Paint_DrawString_EN(DISPLAY_CONTENT_1_PX_X(0), DISPLAY_CONTENT_1_PX_Y(25), data[0].data, &Font16, WHITE, BLACK);
    Paint_DrawString_EN(DISPLAY_CONTENT_1_PX_X(0), DISPLAY_CONTENT_1_PX_Y(45), data[0].person, &Font16, WHITE, BLACK);
    Paint_DrawString_EN(DISPLAY_CONTENT_2_PX_X(0), DISPLAY_CONTENT_2_PX_Y(5), data[1].time, &Font16, WHITE, BLACK);
    Paint_DrawString_EN(DISPLAY_CONTENT_2_PX_X(0), DISPLAY_CONTENT_2_PX_Y(25), data[1].data, &Font16, WHITE, BLACK);
    Paint_DrawString_EN(DISPLAY_CONTENT_2_PX_X(0), DISPLAY_CONTENT_2_PX_Y(45), data[1].person, &Font16, WHITE, BLACK);

    LOG(20, "Draw date: " + String(data->time), LOG_INFO);
}

static void displayData(void)
{   
    EPD_2IN7B_Display(BlackImage, RedImage);

    POWER_MANAGER->addTask(DEEP_SLEEP_MODE, NULL);

    LOG(20, "Display", LOG_INFO);
}

//----------