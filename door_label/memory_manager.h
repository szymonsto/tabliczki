/*
 * Memory manager
 * 
 * Singleton pattern
 */

#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include "Vector.h"
#include "Arduino.h"
#include "app_core.h"

using namespace std;

//----------

#define MEMORY_MANAGER MemoryManager::getInstance()

//----------
struct MemoryChunk
{
    void *pointer;
    int numObjects;
    long long timestamp;
};

class MemoryManager
{      
    public:  
    static MemoryManager* getInstance(void);
    void begin(void);
    void* allocate(int sizeOf, int numObjects);
    void deallocate(void *pointer);
    void process(void);

    private:
    MemoryManager(void);
    static MemoryManager *_instance;
    Vector<struct MemoryChunk>allocationTable;
    void garbageCollector(void);
    struct MemoryChunk vectorStorage[MEMORY_MANAGER_VECTOR_STORAGE_SIZE];
};

//----------

#endif /* MEMORY_MANAGER_H */