/*
 * App config
 */

#ifndef APP_CONFIG_H
#define APP_CONFIG_H

#include <stdlib.h>

//----------
/* Global */

#define APP_ROOM_DATA_LENGTH 30

#define APP_DISPLAY_DATA_LENGTH 100

#define APP_DEVICE_INFO_LENGTH 100

#define APP_USER_DATA_LENGTH 100

//----------
/* Logs */

#define APP_USE_LOGS 1
#define APP_PRINT_LOGS 0 // print log when reporting

/* 
 *  APP_USE_LOGS_COMMAND options:
 *  0 - log command disabled
 *  1 - print log command
 *  2 - save log command
 *  3 - print and save log command
 */
#define APP_USE_LOGS_COMMAND 1

#define APP_LOG_LENGTH 30

//----------
/* Serial port */

#define APP_SERIAL_PORT_BAUDRATE 9600

//----------
/* Memory manager */

#define GARBAGE_COLLECTOR_TIMEOUT 30000

//----------
/* Button */

#define BUTTON_DEBOUNCE_PERIOD 300 /* miliseconds */

//----------

#endif /* APP_COMFIG_H */
