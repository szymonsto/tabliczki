/*
 * RTC
 */

#ifndef RTC_H
#define RTC_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "state.h"

/* Arduino libraries */
#include <ESP8266WiFi.h>
#include "Arduino.h"
#include "cppQueue.h"

using namespace std;

//----------

#define APP_RTC AppRtc::getInstance()

//----------

struct AppRtcTask
{
    enum appRtcTask task;
    void *data;
};

class AppRtc
{
    public:
    static AppRtc* getInstance(void);
    void begin(void);
    void process(void);
    static AppRtc *_instance;

    bool addTask(enum appRtcTask task, void *data);

    private:
    AppRtc(void): taskQueue(sizeof(struct AppRtcTask), APP_RTC_TASK_QUEUE_SIZE, FIFO) { }
    Queue taskQueue;        
};

//----------

#endif /* RTC_H */