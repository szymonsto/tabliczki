/*
 * RTC
 */

#include "rtc.h"

#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "serial_port_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "firebase_manager.h"
#include "app_manager_user_state.h"
#include "app_manager_config_state.h"
#include "hw.h"
#include "device.h"

/* Arduino libraries */
#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "cppQueue.h"

//----------
/* Context */

AppRtc *AppRtc::_instance = NULL;

//----------

static void handleTask(struct SerialTask task);

//----------

AppRtc* AppRtc::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new AppRtc();
  }

  return _instance;
}

void AppRtc::begin(void)
{
    i2cInit();
}

void AppRtc::process(void)
{
    struct SerialTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

bool AppRtc::addTask(enum appRtcTask task, void *data)
{
    struct AppRtcTask newTask = DEF_APP_RTC_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        return false;
    }  
}

//----------

static void handleTask(struct SerialTask task)
{
    LOG(25, "RTC - get time", LOG_INFO);

    rtcGetTimeHw(DEVICE->data.currentTime, APP_USER_DATA_LENGTH);

    APP_MANAGER->addTask(RTC_DATA, NULL);
}

//----------