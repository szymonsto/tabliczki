/*
 * EEPROM
 */

#include "eeprom.h"

#include "app_manager.h"
#include "memory_manager.h"
#include <Eeprom24C32_64.h>

//----------

AppEeprom *AppEeprom::_instance = NULL;

//----------

AppEeprom *AppEeprom::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppEeprom();
    }

    return _instance;
}

void AppEeprom::begin(void)
{ }

void AppEeprom::process(void)
{
    struct AppEepromTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }
}

bool AppDevice::addTask(enum AppEepromTask task, void *data)
{
    struct AppEepromTask newTask = DEF_EEPROM_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        LOG(18, "Add Task failed", LOG_WARNING);
        return false;
    } 
}

void AppEeprom::handleTask(struct AppEepromTask task)
{
    switch(task.task)
    {
        case EEPROM_WRITE:
            eeprom.writeByte(address, *(uint8_t *)task.data);  
        break;
        case EEPROM_READ:
            uint8_t *data = MEMORY_MANAGER->allocate(1, 1);
            *data = eeprom.readByte(address);

            APP_MANAGER->addTask(NEW_EEPROM_DATA, data);  
        break;
        default:
            LOG(18, "Unsupported task", LOG_ERROR);
        break;
    }
}

//----------