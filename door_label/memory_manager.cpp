/*
 * Memory manager
 */

#include "memory_manager.h"

#include "Vector.h"
#include "Arduino.h"

#include "hw.h"
#include "app_config.h"
#include "utils.h"

using namespace std;

//----------
/* Context */

MemoryManager *MemoryManager::_instance = NULL;

//----------
/* Private functions */

static int findPointerInAllocTable(Vector<struct MemoryChunk> v, void *pointer)
{
    for(unsigned int i = 0; i < v.size(); i++)
    {
        if(v[i].pointer == pointer)
        {
            return i;
        }
    }

    return INT_MAX;
}

//----------

MemoryManager::MemoryManager(void)
{ }

MemoryManager* MemoryManager::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new MemoryManager();
    }

    return _instance;
}

void* MemoryManager::allocate(int sizeOf, int numObjects)
{
    void *ptr = malloc(sizeOf * numObjects);

    if(ptr == NULL)
    {
        return 0; //malloc failed
    }

    struct MemoryChunk mem;
    mem.pointer = ptr;
    mem.numObjects = numObjects;
    mem.timestamp = getMillis();

    allocationTable.push_back(mem); 

    return ptr;
}

void MemoryManager::deallocate(void *pointer)
{    
    int pos = findPointerInAllocTable(allocationTable, pointer);

    if(pos == INT_MAX)
    {
        return; // not found
    }

    allocationTable.remove(pos);

    free(pointer);   
}

void MemoryManager::garbageCollector(void)
{
    for(unsigned int i = 0; i < allocationTable.size(); i++)
    {
        if(timeElapsed(allocationTable[i].timestamp, GARBAGE_COLLECTOR_TIMEOUT))
        {
            free(allocationTable[i].pointer);
            allocationTable.remove(i);
        }
    }
}

void MemoryManager::begin(void)
{
    allocationTable.setStorage(vectorStorage);
}

void MemoryManager::process(void)
{
    garbageCollector();
}

//----------
