/*
 * Logs events
 * 
 * Command design pattern
 */

#include "logs_events.h"

#include "logs_manager.h"
#include "app_manager.h"
#include "memory_manager.h"
#include "tasks.h"
#include "command.h"
#include "hw.h"

//----------

AppManagerPrintEvent::AppManagerPrintEvent(void)
{ }

AppManagerPrintEvent::~AppManagerPrintEvent(void)
{ }

void AppManagerPrintEvent::execute(void)
{  
  void *memPtr = MEMORY_MANAGER->allocate(sizeof(LOGS->getData()), 1);

  if(memPtr == NULL)
  {
    return; // allocation failed
  }

  struct logData data = LOGS->getData();

  memcpy(memPtr, &data, sizeof(LOGS->getData()));

  APP_MANAGER->addTask(PRINT_LOG, memPtr);
}

AppManagerSaveEvent::AppManagerSaveEvent(void)
{ }

AppManagerSaveEvent::~AppManagerSaveEvent(void)
{ }

void AppManagerSaveEvent::execute(void)
{  
  void *memPtr = MEMORY_MANAGER->allocate(sizeof(LOGS->getData()), 1);

  if(memPtr == NULL)
  {
    return; // allocation failed
  }

  struct logData data = LOGS->getData();

  memcpy(memPtr, &data, sizeof(LOGS->getData()));

  APP_MANAGER->addTask(SAVE_LOG, memPtr);
}

AppManagerPrintSaveEvent::AppManagerPrintSaveEvent()
{ }

AppManagerPrintSaveEvent::~AppManagerPrintSaveEvent(void)
{ }

void AppManagerPrintSaveEvent::execute(void)
{  
  void *memPtr = MEMORY_MANAGER->allocate(sizeof(LOGS->getData()), 1);

  if(memPtr == NULL)
  {
    return; // allocation failed
  }

  struct logData data = LOGS->getData();

  memcpy(memPtr, &data, sizeof(LOGS->getData()));

  APP_MANAGER->addTask(PRINT_LOG, memPtr);
  
  APP_MANAGER->addTask(SAVE_LOG, memPtr);
}

//----------