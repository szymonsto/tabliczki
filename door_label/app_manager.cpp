/*
 * App manager
 */

#include "app_manager.h"

#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "serial_port_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "firebase_manager.h"
#include "app_manager_user_state.h"
#include "app_manager_config_state.h"

/* Arduino libraries */
#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "cppQueue.h"

//----------
/* Context */

AppManager *AppManager::_instance = NULL;

//----------

AppManager* AppManager::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new AppManager();
  }

  return _instance;
}

void AppManager::begin(void)
{
  _state = APP_MANAGER_STATE_USER;

  _state->begin();
}

void AppManager::process(void)
{
  _state->process();
}

bool AppManager::addTask(enum appManagerTask task, void *data)
{
  return _state->addTask(task, data);
}

void AppManager::transitionToState(State *state) 
{    
    _state = state;
    _state->setContext(this);  
    _state->transitionSetup();
}

//----------