/*
 * App manager
 */

#include "app_manager_config_state.h"

#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "serial_port_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "firebase_manager.h"
#include "app_manager.h"
#include "utils.h"
#include "app_manager_user_state.h"
#include "display_manager.h"
#include "device.h"

/* Arduino libraries */
#include "Arduino.h"
#include "cppQueue.h"

//----------
/* Context */

AppManagerStateConfig *AppManagerStateConfig::_instance = NULL;

//----------

#define DEBUG_MODE_STOP_SEQUENCE "stop"

enum configCommand
{
  AP_SSID_CMD,
  AP_PASSWD_CMD,
  DEV_NAME_CMD,
  DEV_PASSWD_CMD,
  BASE_KEY_CMD,
  BASE_AUTH_CMD,
  ROOM_CMD,
};

static char *configCommandTab[] = 
{
  [AP_SSID_CMD] = "ap_ssid",
  [AP_PASSWD_CMD] = "ap_passwd",
  [DEV_NAME_CMD] = "dev_name",
  [DEV_PASSWD_CMD] = "dev_passwd",
  [BASE_KEY_CMD] = "base_key",
  [BASE_AUTH_CMD] = "base_auth",
  [ROOM_CMD] = "room",
};

static char *sLogTypes[] = 
{
  [LOG_INFO] = "INFO: ",
  [LOG_WARNING] = "WARNING: ",
  [LOG_ERROR] = "ERROR: ",
};

//----------

static void printLogRequest(void *data);
static void printEchoRequest(void *data);
static String logToString(struct logData newLog);
static enum configCommand getConfigCommand(String data);
static String getConfigArgument(String data);
static void handleConfigCommand(String data);
void sendAck(void);

//----------

void AppManagerStateConfig::begin(void)
{
  SERIAL_PORT->begin();
  APP_WIFI->begin();  
  APP_FIREBASE->begin();

  LOG(1, "Successful initialization", LOG_INFO);
}

void AppManagerStateConfig::process(void)
{
  SERIAL_PORT->process();
  LOGS->process();
  MEMORY_MANAGER->process();  
  APP_WIFI->process();
  APP_FIREBASE->process();
  APP_DISPLAY->process();
}

bool AppManagerStateConfig::addTask(enum appManagerTask task, void *data)
{
  switch(task)
  {
    case PRINT_LOG:
      printLogRequest(data);
    break;
    case SAVE_LOG:
      //not supported
    break;
    case WIFI_CONNECTED:
      LOG(10, "Wifi - connected", LOG_INFO); 
      MEMORY_MANAGER->deallocate(data);
    break;
    case NEW_SERIAL_DATA:
      handleSerialData(data);      
      MEMORY_MANAGER->deallocate(data);
    break;
    default:
      LOG(10, "Unsupported task", LOG_WARNING); 
    break;
  }

  return false;
}

void AppManagerStateConfig::handleSerialData(void *data)
{
  if(findSubstring(String((char *)data), String(DEBUG_MODE_STOP_SEQUENCE)))
  {
    LOG(15, "User mode", LOG_INFO);
    APP_MANAGER->transitionToState(APP_MANAGER_STATE_USER);
  }

  handleConfigCommand(String((char *)data));
}

AppManagerStateConfig* AppManagerStateConfig::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new AppManagerStateConfig();
  }

  return _instance;
}

void AppManagerStateConfig::transitionSetup(void)
{
    LOG(10, "Config - transition setup", LOG_INFO);
}

//----------

static void printLogRequest(void *data)
{
  String sLog = logToString(*(struct logData *)data);

  MEMORY_MANAGER->deallocate(data);

  void *newLog = MEMORY_MANAGER->allocate(sizeof(char), sLog.length() + 1);

  sLog.getBytes((unsigned char *)newLog, sLog.length() + 1);

  SERIAL_PORT->addTask(WRITE, newLog);
}

static String logToString(struct logData newLog)
{
  String sLog = sLogTypes[newLog.type];

  sLog.concat(newLog.description);

  return sLog;
}

static void printEchoRequest(void *data)
{
  String sEcho = "ECHO: ";

  sEcho += String((char *)data);

  void *newEcho = MEMORY_MANAGER->allocate(sizeof(char), sEcho.length() + 1);

  sEcho.getBytes((unsigned char *)newEcho, sEcho.length() + 1);

  SERIAL_PORT->addTask(WRITE, newEcho);
}

static enum configCommand getConfigCommand(String data)
{
  String cmd = data.substring(0, findStringChar(data, ' '));

  for(int i = 0; i < 7; i++)
  {
    if(findSubstring(cmd, configCommandTab[i]))
    {
      return (enum configCommand)i;
    }
  }

  return (enum configCommand)INT_MAX;
}

static String getConfigArgument(String data)
{
  return data.substring(findStringChar(data, ' ') + 1); /* Note: + 1 -> because of ' ' character */
}

static void handleConfigCommand(String data)
{
  switch(getConfigCommand(data))
  {
    case AP_SSID_CMD:
      sendAck();
      DEVICE->data.apSsid = getConfigArgument(data);
    break;
    case AP_PASSWD_CMD:
      sendAck();
      DEVICE->data.apPasswd = getConfigArgument(data);
    break;
    case DEV_NAME_CMD:
      sendAck();
      DEVICE->data.devSsid = getConfigArgument(data);
    break;
    case DEV_PASSWD_CMD:
      sendAck();
      DEVICE->data.devPasswd = getConfigArgument(data);
    break;
    case BASE_KEY_CMD:
      sendAck();
      DEVICE->data.baseDomain = getConfigArgument(data);
    break;
    case BASE_AUTH_CMD:
      sendAck();
      DEVICE->data.baseAuth = getConfigArgument(data);
    break;
    case ROOM_CMD:
      sendAck();
      DEVICE->data.room = getConfigArgument(data);
    break;
    default:
      LOG(10, "Command not found", LOG_WARNING);
    break;
  }
}

void sendAck(void)
{
  String sAck = "ack";
  void *newAck = MEMORY_MANAGER->allocate(sizeof(char), sAck.length() + 1);

  sAck.getBytes((unsigned char *)newAck, sAck.length() + 1);
  SERIAL_PORT->addTask(WRITE, newAck);
}

//----------
