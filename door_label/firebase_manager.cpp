/*
 * Firebase manager
 */

#include "firebase_manager.h"

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "app_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "device.h"

#include <ESP8266WiFi.h>
#include "Arduino.h"
#include <FirebaseArduino.h>
#include <cppQueue.h>
#include "Vector.h"

//----------
/* Context */

AppFirebase *AppFirebase::_instance = NULL;

static String propertyTab[] = 
{
  [C_NAMES] = "c_names",
  [HOUR_BEG] = "hours/0",
  [HOUR_END] = "hours/1",
  [MAILS] = "mails",
  [ROOM] = "room",
  [T_NAMES] = "t_names",
  [UPDATE_DATE] = "update_date/0",
  [UPDATE_HOUR] = "update_date/1",
  [WEEKDAY] = "weekday",
};

static String hourTab[] = 
{
  [HOUR_8_00] = "8:00",
  [HOUR_9_35] = "9:35",
  [HOUR_11_15] = "11:15",
  [HOUR_12_50] = "12:50",
  [HOUR_14_40] = "14:40",
  [HOUR_16_15] = "16:15",
  [HOUR_17_50] = "17:50",
  [HOUR_19_25] = "19:25",  
};

static String weekdayTab[] = 
{
  [MONDAY] = "Monday",
  [TUESDAY] = "Tuesday",
  [WEDNESDAY] = "Wednesday",
  [THURSDAY] = "Thursday",
  [FRIDAY] = "Friday",
};

//----------

static void handleTask(struct AppFirebaseTask task);
static void reportEvent(void);
static void refreshRoomData(String room);
static void scanFirebase(String room);
static void checkUpdateDate(struct RoomData *data);
static int compareDate(String date1, String date2);
static int compareTime(String time1, String time2);
static struct date stringToDate(String date);
static struct time stringToTime(String date);
static String roomNodePath(String room, String weekday, String hour, String property);
static void sendData(void);

//----------

AppFirebase *AppFirebase::getInstance(void)
{
    if(_instance == NULL)
    {
        _instance = new AppFirebase();
    }

    return _instance;
}

void AppFirebase::setHostData(String firebaseHost, String firebaseAuth)
{
    _data = DEF_FIREBASE_DATA_OBJ(firebaseHost, firebaseAuth);
}

void AppFirebase::connect(void)
{
    Firebase.begin(_data.firebaseHost, _data.firebaseAuth);

    Serial.println("KEY: " + _data.firebaseHost + " " + _data.firebaseAuth);
}

void AppFirebase::disconnect(void)
{ }

void AppFirebase::begin(void)
{
  _roomsData.setStorage(vectorStorage);
}

void AppFirebase::process(void)
{
    struct AppFirebaseTask newTask;

    while(taskQueue.pop(&newTask))
    {
        handleTask(newTask);
    }

    reportEvent();
}

bool AppFirebase::addTask(enum appFirebaseTask task, void *data)
{
    struct AppFirebaseTask newTask = DEF_FIREBASE_TASK_OBJ(task, data); 

    if(taskQueue.push(&newTask))
    {
        return true;
    }
    else
    {
        LOG(8, "Add Task failed", LOG_WARNING);
        return false;
    } 
}

struct RoomData AppFirebase::getRoomData(int id)
{
    return _roomsData[id];
}

//----------

static void handleTask(struct AppFirebaseTask task)
{
    switch(task.task)
    {
        case BEGIN:
            APP_FIREBASE->_state.connectionRequest = true;            
        break;
        case REFERSH_ROOM_DATA:
            APP_FIREBASE->_state.refreshRequest = true; 
            APP_FIREBASE->_roomData = DEVICE->data.room;            
            MEMORY_MANAGER->deallocate(task.data);
        break;
        case PUSH_ESP_DATA:
        break;
        case FIREBASE_GET_ROOM_DATA:
            sendData();
        break;
        default:
            LOG(8, "Unsupported task", LOG_WARNING);
        break;
    }
}

static void reportEvent(void)
{
    if(APP_FIREBASE->_state.connectionRequest && WiFi.status() == WL_CONNECTED)
    {
        LOG(8, "Firebse Task - connect", LOG_INFO);
        APP_FIREBASE->connect();

        Serial.println("Firebase connected");

        APP_FIREBASE->_state.connectionRequest = false;
        APP_FIREBASE->_state.connected = true;
    }

    if(APP_FIREBASE->_state.connected && APP_FIREBASE->_state.refreshRequest)
    {
        Serial.println("BASE: refresh request");
        LOG(8, "Task - refresh", LOG_INFO);
        refreshRoomData(APP_FIREBASE->_roomData);
        APP_FIREBASE->_state.refreshRequest = false;
    }    
}

static void refreshRoomData(String room)
{
    scanFirebase(room);
}

static void scanFirebase(String room)
{
    static struct RoomData data;
    String temp;
    char emptyString[APP_ROOM_DATA_LENGTH];
    setChar(emptyString, "", APP_ROOM_DATA_LENGTH);

    APP_FIREBASE->_roomsData.clear();

    for(int day = 0; day < 5; day++) // TODO: array size
        for(int hour = 0; hour < 8; hour++) 
        {
            temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[UPDATE_DATE]));
            temp.getBytes((unsigned char *)data.UpdateDate, APP_ROOM_DATA_LENGTH);         

            if(String(data.UpdateDate) != String(emptyString))
            {
                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[UPDATE_HOUR]));
                temp.getBytes((unsigned char *)data.UpdateHour, APP_ROOM_DATA_LENGTH);
                
                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[WEEKDAY]));
                temp.getBytes((unsigned char *)data.weekday, APP_ROOM_DATA_LENGTH);

                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[HOUR_BEG]));
                temp.getBytes((unsigned char *)data.hoursBeg, APP_ROOM_DATA_LENGTH);

                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[C_NAMES]));
                temp.getBytes((unsigned char *)data.c_names, APP_ROOM_DATA_LENGTH);

                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[HOUR_END]));
                temp.getBytes((unsigned char *)data.hoursEnd, APP_ROOM_DATA_LENGTH);

                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[MAILS]));
                temp.getBytes((unsigned char *)data.mails, APP_ROOM_DATA_LENGTH);

                temp = Firebase.getString(roomNodePath(room, weekdayTab[day], hourTab[hour], propertyTab[T_NAMES]));
                temp.getBytes((unsigned char *)data.t_names, APP_ROOM_DATA_LENGTH);

                temp = weekdayTab[day];
                temp.getBytes((unsigned char *)data.weekdayFromPath, APP_ROOM_DATA_LENGTH);
                
                temp = hourTab[hour];
                temp.getBytes((unsigned char *)data.hoursBegFromPath, APP_ROOM_DATA_LENGTH);

                APP_FIREBASE->_roomsData.push_back(data);
            }
        }
    }
    int *taskData = (int *)MEMORY_MANAGER->allocate(sizeof(int), 1);

    *taskData = APP_FIREBASE->_roomsData.size();

    APP_MANAGER->addTask(FIREBASE_SCAN_END, (void *)taskData);
}

static void checkUpdateDate(struct RoomData *data)
{
    for(int i = 0; i < APP_FIREBASE->_roomsData.size(); i++)
    {
        if((APP_FIREBASE->_roomsData[i].weekday == data->weekday) && (APP_FIREBASE->_roomsData[i].hoursBeg == data->hoursBeg))
        {
            if((compareTime(APP_FIREBASE->_roomsData[i].UpdateHour, data->UpdateHour) > 0) && (compareDate(APP_FIREBASE->_roomsData[i].UpdateDate, data->UpdateDate) >= 0))
            {
                memcpy(APP_FIREBASE->_roomsData[i].c_names, data->c_names, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].hoursBeg, data->hoursBeg, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].hoursEnd, data->hoursEnd, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].mails, data->mails, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].room, data->room, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].t_names, data->t_names, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].UpdateDate, data->UpdateDate, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].UpdateHour, data->UpdateHour, APP_ROOM_DATA_LENGTH);
                memcpy(APP_FIREBASE->_roomsData[i].weekday, data->weekday, APP_ROOM_DATA_LENGTH);

                return;
            }
        }
    }

    APP_FIREBASE->_roomsData.push_back(*data);
}

static int compareDate(String date1, String date2)
{
    struct date date1T = stringToDate(date1);
    struct date date2T = stringToDate(date2);

    if((date1T.year >= date2T.year) && (date1T.month >= date2T.month) && (date1T.day > date2T.day))
    {
        return 1;
    }
    else if((date1T.year == date2T.year) && (date1T.month == date2T.month) && (date1T.day == date2T.day))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

static int compareTime(String time1, String time2)
{
    struct time time1T = stringToTime(time1);
    struct time time2T = stringToTime(time2);

    if((time1T.hour >= time2T.hour) && (time1T.min >= time2T.min) && (time1T.sec > time2T.sec))
    {
        return 1;
    }
    else if((time1T.hour == time2T.hour) && (time1T.min == time2T.min) && (time1T.sec == time2T.sec))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

static struct date stringToDate(String date)
{
    int end;
    struct date result;

    for(int i = 0; i < date.length(); i++)
    {
        if(date[i] == '-')
        {
            result.year = date.substring(0, i).toInt();
            end = i;
            break;
        }
    }

    for(int i = end+1; i < date.length(); i++)
    {
        if(date[i] == '-')
        {
            result.month = date.substring(end+1, i).toInt();
            end = i;
            break;
        }
    }

    for(int i = end+1; i < date.length(); i++)
    {
        if(date[i] == '-')
        {
            result.day = date.substring(end+1, i).toInt();
            break;
        }
    }

    return result;
}

static struct time stringToTime(String date)
{
    int end;
    struct time result;

    for(int i = 0; i < date.length(); i++)
    {
        if(date[i] == ':')
        {
            result.hour = date.substring(0, i).toInt();
            end = i;
            break;
        }
    }

    for(int i = end+1; i < date.length(); i++)
    {
        if(date[i] == ':')
        {
            result.min = date.substring(end+1, i).toInt();
            end = i;
            break;
        }
    }

    for(int i = end+1; i < date.length(); i++)
    {
        if(date[i] == ':')
        {
            result.sec = date.substring(end+1, i).toInt();
            break;
        }
    }

    return result;
}

static String roomNodePath(String room, String weekday, String hour, String property)
{
    return "/Room/" + room + "/" + weekday + "/" + hour + "/" + property;
}

static void sendData(void)
{
    void *taskData = MEMORY_MANAGER->allocate(sizeof(struct RoomData), APP_FIREBASE->_roomsData.size());

    for(int i = 0; i < APP_FIREBASE->_roomsData.size(); i++)
    {
        memcpy((((struct RoomData *)taskData) + i), &(APP_FIREBASE->_roomsData[i]), sizeof(struct RoomData));
    }

    APP_MANAGER->addTask(FIREBASE_DATA, taskData);
}

//----------