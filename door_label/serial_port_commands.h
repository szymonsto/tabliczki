/*
 * serial_port_cmmands.h
 */

#ifndef SERIAL_PORT_COMMANDS_H
#define SERIAL_PORT_COMMANDS_H

#include <stdlib.h>

#include "app_core.h"
#include "app_config.h"
#include "command.h"

//----------

class SerialPortWriteCommand : public Command
{
    public:
    SerialPortWriteCommand(void);  
    ~SerialPortWriteCommand(void);  
    void execute(void);
};

class SerialPortReadCommand : public Command
{
    public:
    SerialPortReadCommand(void); 
    ~SerialPortReadCommand(void);
    void execute(void); 
};

class SerialPortWaitForCommand : public Command
{
    public:
    SerialPortWaitForCommand(void);
    ~SerialPortWaitForCommand(void);  
    void execute(void); 
};

//----------

#endif /* SERIAL_PORT_COMMANDS_H */