/*
 * app_manager.h
 * 
 * Singleton design pattern
 */

#ifndef APP_MANAGER_H
#define APP_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"
#include "state.h"

/* Arduino libraries */
#include <ESP8266WiFi.h>
#include "Arduino.h"
#include "cppQueue.h"

using namespace std;

//----------

#define APP_MANAGER AppManager::getInstance()

//----------

class State;

struct AppManagerTask
{
    enum appManagerTask task;
    void *data;
};

class AppManager
{
    public:
    static AppManager* getInstance(void);
    void begin(void);
    void process(void);
    static AppManager *_instance;

    bool addTask(enum appManagerTask task, void *data);
    void transitionToState(State *state);

    private:
    AppManager(void): taskQueue(sizeof(struct AppManagerTask), APP_MANAGER_TASK_QUEUE_SIZE, FIFO) { }
    Queue taskQueue;    
    State *_state; 
    
};

//----------

#endif /* APP_MANAGER_H */