/*
 * App manager
 */

#include "button.h"

#include "tasks.h"
#include "logs_manager.h"
#include "memory_manager.h"
#include "serial_port_manager.h"
#include "app_core.h"
#include "wifi_manager.h"
#include "firebase_manager.h"
#include "app_manager_user_state.h"
#include "app_manager_config_state.h"
#include "hw.h"
#include "utils.h"

/* Arduino libraries */
#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "cppQueue.h"

//----------
/* Context */

AppButton *AppButton::_instance = NULL;

//----------

ICACHE_RAM_ATTR void handleInterruptBut1(void);
ICACHE_RAM_ATTR void handleInterruptBut2(void);
ICACHE_RAM_ATTR void handleInterruptBut3(void);

//----------

struct AppButtonCtx
{
  long long timestamp;
  bool state;
};

static struct AppButtonCtx appButtonCtxTab[3];

static enum appManagerTask appTaskTab[] = 
{
  BUTTON_LEFT_PRESS,
  BUTTON_MID_PRESS,
  BUTTON_RIGHT_PRESS,
};

//----------

AppButton* AppButton::getInstance(void)
{
  if(_instance == NULL)
  {
    _instance = new AppButton();
  }

  return _instance;
}

void AppButton::begin(void)
{
  pinMode(PIN_BUT1, INPUT); 
  attachInterrupt(digitalPinToInterrupt(PIN_BUT1), handleInterruptBut1, CHANGE);

  pinMode(PIN_BUT2, INPUT); 
  attachInterrupt(digitalPinToInterrupt(PIN_BUT2), handleInterruptBut2, CHANGE);

  pinMode(PIN_BUT3, INPUT); 
  attachInterrupt(digitalPinToInterrupt(PIN_BUT3), handleInterruptBut3, CHANGE);
}

void AppButton::process(void)
{
  for(int i = 0; i < 3; i++)
  {
    if((appButtonCtxTab[i].state == true) && timeElapsed(appButtonCtxTab[i].timestamp, BUTTON_DEBOUNCE_PERIOD))
    {
      Serial.println("Button interrupt process: " + String(i));

      appButtonCtxTab[i].state = false;

      APP_MANAGER->addTask(appTaskTab[i], NULL);
    }
  }
}

//----------

void handleInterruptBut1(void)
{
  if((appButtonCtxTab[0].state == false) && timeElapsed(appButtonCtxTab[0].timestamp, BUTTON_DEBOUNCE_PERIOD))
  {
    appButtonCtxTab[0].state = true;
    appButtonCtxTab[0].timestamp = getMillis();
  }
}

void handleInterruptBut2(void)
{
  if((appButtonCtxTab[1].state == false) && timeElapsed(appButtonCtxTab[1].timestamp, BUTTON_DEBOUNCE_PERIOD))
  {
    appButtonCtxTab[1].state = true;
    appButtonCtxTab[1].timestamp = getMillis();
  }
}

void handleInterruptBut3(void)
{
  if((appButtonCtxTab[2].state == false) && timeElapsed(appButtonCtxTab[2].timestamp, BUTTON_DEBOUNCE_PERIOD))
  {
    appButtonCtxTab[2].state = true;
    appButtonCtxTab[2].timestamp = getMillis();
  }
}

//----------