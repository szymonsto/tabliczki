/*
 * App manager state
 */

#ifndef APP_MANAGER_CONFIG_STATE_H
#define APP_MANAGER_CONFIG_STATE_H

#include "state.h"

#include "app_manager.h"

//----------

#define APP_MANAGER_STATE_CONFIG AppManagerStateConfig::getInstance()

//----------

class AppManagerStateConfig: public State
{
    public:
    void begin(void);
    void process(void);
    bool addTask(enum appManagerTask task, void *data);
    static AppManagerStateConfig* getInstance(void);
    static AppManagerStateConfig *_instance;

    void transitionSetup(void);

    private:
    void handleSerialData(void *data);
    AppManagerStateConfig() {}
    ~AppManagerStateConfig() {}
};

//----------

#endif /* APP_MANAGER_CONFIG_STATE_H */

