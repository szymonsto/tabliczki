/*
 * Tasks
 */

#ifndef TASKS_H
#define TASKS_H

//----------
/* App manager */

#define APP_MANAGER_TASK_QUEUE_SIZE 10

enum appManagerTask
{
    PRINT_LOG,
    SAVE_LOG,
    WIFI_CONNECTED,
    NEW_SERIAL_DATA,
    FIREBASE_SCAN_END,
    FIREBASE_DATA,
    RTC_DATA,
    WIFI_CONNECTION_REQUEST_CLIENT,
    WIFI_CONNECTION_REQUEST_CLIENT_SERVER,
    FIREBASE_CONNECTION_REQUST,
    BUTTON_LEFT_PRESS,
    BUTTON_MID_PRESS,
    BUTTON_RIGHT_PRESS,
};

//----------
/* Serial port */

#define SERIAL_TASK_QUEUE_SIZE 100

enum serialPortTask
{
    WRITE,
    READ,
    WAIT_FOR,
};

//----------
/* Wifi */

#define WIFI_TASK_QUEUE_SIZE 10

enum appWifiTasks
{
    CONNECT,
};

//----------
/* Firebase */

#define FIREBASE_TASK_QUEUE_SIZE 10

enum appFirebaseTask
{
    BEGIN,
    REFERSH_ROOM_DATA,
    PUSH_ESP_DATA,
    FIREBASE_GET_ROOM_DATA,
};

//----------
/* Display */

#define DISPLAY_TASK_QUEUE_SIZE 20

enum appDisplayTask
{
    CLEAR,
    DRAW_BAR,
    DRAW_CONTENT,
    DISPLAY_DATA,
};

//----------
/* RTC */

#define APP_RTC_TASK_QUEUE_SIZE 10

enum appRtcTask
{
    GET_TIME,
};

//----------
/* Device */

#define APP_DEVICE_TASK_QUEUE_SIZE 10

enum appDeviceTask
{
    WIFI_CONNECTED_DEVICE,
    BASE_CONNECTED_DEVICE,
};

//----------
/* Power manager */

#define APP_POWER_MANAGER_TASK_QUEUE_SIZE 10

enum appPowerManagerTask
{
    DEEP_SLEEP_MODE,
};

//----------
/* EEPROM */

#define APP_EEPROM_TASK_QUEUE_SIZE 10

enum appEepromTask
{
    EEPROM_WRITE,
    EEPROM_READ,
};


//----------

#endif /* TASKS_H */