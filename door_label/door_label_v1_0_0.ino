#include <app_config.h>
#include <app_core.h>
#include <app_manager.h>
#include <command.h>
#include <hw.h>
#include <logs_events.h>
#include <logs_manager.h>
#include <memory_manager.h>
#include <serial_port_commands.h>
#include <serial_port_events.h>
#include <serial_port_manager.h>
#include <tasks.h>
#include <utils.h>
#include <wifi_manager.h>

void setup() { 
  
  APP_MANAGER->begin();  
}

void loop() {

  APP_MANAGER->process();
  
}