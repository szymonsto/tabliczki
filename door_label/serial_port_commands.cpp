/*
 * Serial port commands
 * 
 * Command design pattern
 */

#include "serial_port_commands.h"

#include "serial_port_manager.h"
#include "logs_manager.h"
#include "app_manager.h"
#include "memory_manager.h"
#include "tasks.h"
#include "command.h"

//----------

SerialPortWriteCommand::SerialPortWriteCommand(void)
{ }

SerialPortWriteCommand::~SerialPortWriteCommand(void)
{ }

void SerialPortWriteCommand::execute(void)
{  
    void *memPtr = MEMORY_MANAGER->allocate(sizeof(SERIAL_PORT->getData()), 1);

    if(memPtr == NULL)
    {
        return; //allocation failed
    }

    struct SerialTask data = SERIAL_PORT->getData();

    memcpy(memPtr, &data, sizeof(SERIAL_PORT->getData()));

    SERIAL_PORT->addTask(WRITE, memPtr);
}

SerialPortReadCommand::SerialPortReadCommand(void)
{ }

SerialPortReadCommand::~SerialPortReadCommand(void)
{ }

void SerialPortReadCommand::execute(void)
{  }

SerialPortWaitForCommand::SerialPortWaitForCommand()
{ }

SerialPortWaitForCommand::~SerialPortWaitForCommand(void)
{ }

void SerialPortWaitForCommand::execute(void)
{  }

//----------