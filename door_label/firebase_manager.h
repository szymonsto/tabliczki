/*
 * Firebase manager
 */

#ifndef FIREBASE_MANAGER_H
#define FIREBASE_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

#include "Arduino.h"
#include <cppQueue.h>
#include <Vector.h>

//----------

#define APP_FIREBASE AppFirebase::getInstance()

//----------

struct AppFirebaseTask
{
    enum appFirebaseTask task;
    void *data;
};

struct AppFirebaseHostData
{
    String firebaseHost;
    String firebaseAuth;
};

struct AppFirebaseState
{
    bool connectionRequest;
    bool isReadyToConnect;
    bool connected;
    bool refreshRequest;
};

class AppFirebase
{
    public:
    static AppFirebase* getInstance(void);
    void connect(void);
    void disconnect(void);
    void setHostData(String firebaseHost, String firebaseAuth);
    void begin(void);
    void process(void);
    bool addTask(enum appFirebaseTask task, void *data);
    struct AppFirebaseState _state; // TODO: move to private section
    Vector<struct RoomData>_roomsData; // TODO: move to private section
    String _roomData; // TODO: move to private section
    struct RoomData getRoomData(int id);

    private:
    AppFirebase(void): taskQueue(sizeof(struct AppFirebaseTask), FIREBASE_TASK_QUEUE_SIZE, FIFO){ };
    static AppFirebase *_instance;    
    Queue taskQueue; 
    struct AppFirebaseHostData _data;   
    struct RoomData vectorStorage[FIREBASE_VECTOR_STORAGE_SIZE];
};

//----------

#endif /* FIREBASE_MANAGER_H */