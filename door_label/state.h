/*
 * State
 */

#ifndef STATE_H
#define STATE_H

#include "app_manager.h"

//----------

class AppManager;

class State
{
    public:
    void setContext(AppManager *context)
    {
        _context = context;
    }

    virtual void begin(void) = 0;
    virtual void process(void) = 0;
    virtual bool addTask(enum appManagerTask task, void *data) = 0;
    virtual void transitionSetup(void) = 0;

    AppManager *_context;

    virtual ~State() { }
};

//----------

#endif /* STATE_H */