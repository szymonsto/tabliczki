/*
 * Wifi manager
 */

#ifndef WIFI_MANAGER_H
#define WIFI_MANAGER_H

#include "app_core.h"
#include "app_config.h"
#include "command.h"
#include "tasks.h"
#include "logs_manager.h"

#include "Arduino.h"
#include <cppQueue.h>

//----------

#define APP_WIFI AppWifi::getInstance()

//----------

struct AppWifiTask
{
    enum appWifiTasks task;
    void *data;
};

struct AppWifiData
{
    String SSID;  
    String password; 
};

struct AppWifiState
{
    bool isInitialized; // AP data is ready
    bool newConnectionRequest;
};

class AppWifi
{
    public:
    static AppWifi *getInstance(void);
    void setAPData(String ssid, String password);
    void setOwnData(String ssid, String password);
    void setMode(enum wifiManagerMode mode);
    void connect(void);
    void disconnect(void);
    int getStatus(void);
    bool isReadyToConnect(void);
    void begin(void);
    void process(void);
    bool addTask(enum appWifiTasks task, void *data);
    struct AppWifiState _state; 

    private:
    AppWifi(void): taskQueue(sizeof(struct AppWifiTask), WIFI_TASK_QUEUE_SIZE, FIFO){ };
    struct AppWifiData _ownData;
    struct AppWifiData _apData; // access point
    static AppWifi *_instance;    
    Queue taskQueue; 
    
};

//----------

#endif /* WIFI_MANAGER_H */

